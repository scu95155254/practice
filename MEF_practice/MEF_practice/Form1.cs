﻿using System;
using System.Collections.Generic;
using PokamonData;
using System.Windows.Forms;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using 練習MEF參考抽象類別;
using DownloadSave;

namespace MEF_practice
{
    public partial class Form1 : Form
    {
        //[ImportMany]
        //public IEnumerable<IPokamonData> PokamonData { get; set; }


        //[Import]
        //public Class1 Class1 { get; set; }

        [Import]
        public WebCrawler webCrawler { get; set; }
        public Form1()
        {
            InitializeComponent();
            Compose();
        }

        private void comboBox1_Click(object sender, EventArgs e)
        {
            //List<string> comboBoxRow = new List<string>();
            //foreach (var pokamon in PokamonData)
            //{
            //    comboBoxRow.Add(pokamon.Name);
            //}
            //comboBox1.DataSource = comboBoxRow;

        }
        public void Compose()
        {
            DirectoryCatalog catalog = new DirectoryCatalog(Environment.CurrentDirectory, "*.dll");
            CompositionContainer container = new CompositionContainer(catalog);
            try
            {
                container.ComposeParts(this);
            }catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
       
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine(webCrawler.Url);
            Console.WriteLine(webCrawler.TableName);
            //foreach(var webcrawler in WebCrawler)
            //{
            //    Console.Write(webcrawler.Url);
            //    Console.Write(webcrawler.TableName);
            //}

            //Class1.Flow();
            //foreach (var pokamon in PokamonData)
            //{
            //    if (pokamon.Name == comboBox1.Text)
            //    {
            //        textBox1.Text += $"{pokamon.Id}\r\n{pokamon.Name}\r\n{pokamon.Attributes}\r\n{pokamon.Skill}\r\n";
            //    }
            //}

        }
    }
}

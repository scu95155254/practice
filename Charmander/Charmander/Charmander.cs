﻿using PokamonData;
using System.ComponentModel.Composition;

namespace Charmander
{
    [Export("小火龍")]
    public class Charmander : IPokamonData
    {
        public string Name
        {
            get
            {
                return "小火龍";
            }
        }

        public string Id
        {
            get
            {
                return "004";
            }
        }

        public string Attributes
        {
            get
            {
                return "火";
            }
        }
        public string Skill
        {
            get
            { return "火焰噴射"; }
            set
            {
                Skill = "123";
            }
        }
    }
}

﻿using System.ComponentModel.Composition;

namespace 練習MEF參考抽象類別
{
    [InheritedExport]
    public abstract class Class1
    {
        public string Url { get; protected set; }

        public string Name { get; protected set; }

        public bool Flow()
        {
            Download(Url);
            return true;
        }

        public abstract void Download(string url);

    }
}

﻿using System.Text;
using System.Xml;

namespace WriteXMLFile
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "Pokemon.xml";
            XmlTextWriter xmlTextWriter = new XmlTextWriter(fileName, Encoding.UTF8);

            xmlTextWriter.Formatting = Formatting.Indented;

            xmlTextWriter.WriteStartDocument();

            xmlTextWriter.WriteComment("Creating an XML file using C#");

            xmlTextWriter.WriteStartElement("寶可夢");
            xmlTextWriter.WriteElementString("姓名", "妙蛙種子");
            xmlTextWriter.WriteElementString("圖鑑編號", "001");
            xmlTextWriter.WriteElementString("屬性", "草/毒");
            xmlTextWriter.WriteElementString("招式", "飛葉快刀");
            xmlTextWriter.WriteElementString("姓名", "小火龍");
            xmlTextWriter.WriteElementString("圖鑑編號", "004");
            xmlTextWriter.WriteElementString("屬性", "火");
            xmlTextWriter.WriteElementString("招式", "火焰球");
            xmlTextWriter.WriteElementString("姓名", "傑尼龜");
            xmlTextWriter.WriteElementString("圖鑑編號", "007");
            xmlTextWriter.WriteElementString("屬性", "水");
            xmlTextWriter.WriteElementString("招式", "水柱");
            xmlTextWriter.WriteEndElement();
            xmlTextWriter.WriteEndDocument();
            xmlTextWriter.Flush();
            xmlTextWriter.Close();
        }
    }
}

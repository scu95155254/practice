﻿using 練習MEF參考抽象類別;
using System.ComponentModel.Composition;
using System.Net;
using System;
using System.Text;

namespace test20201106
{
    public class Test20201106 : Class1
    {
        public Test20201106()
        {
            this.Url = "https://www.sitca.org.tw/ROC/Industry/IN2107.aspx?pid=IN2213_03";
            this.Name = "test1106";
        }

        public override void Download(string url)
        {
            using (WebClient webClient=new WebClient())
            {
                webClient.Encoding = Encoding.UTF8;
                string data=webClient.DownloadString(url);
                Console.WriteLine(data);
                Console.WriteLine("test123");
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        List<int> Cards;

        public Form1()
        {
            InitializeComponent();
            SetCards();
        }

        private void SetCards()
        {
            Cards = new List<int>();
            for (int i = 0; i < 52; i++)
            {
                Cards.Add(i + 1);
            }
        }

        private void Btn_Shuffle_Click(object sender, EventArgs e)
        {
            Shuffle();
        }

        private void Btn_Licensing_Click(object sender, EventArgs e)
        {
            Shuffle();
            if (int.TryParse(textBox_Number.Text, out int number))
            {
                int playerCards = Cards.Count / number;
                dataGridView1.ColumnCount= number;

                for(int i = 0; i < dataGridView1.Columns.Count; i++)
                {
                    dataGridView1.Columns[i].HeaderCell.Value = $"玩家{i+1}";
                    dataGridView1.RowCount = playerCards;
                    for (int j=0;j< playerCards; j++)
                    {
                        dataGridView1.Rows[j].Cells[i].Value = Cards[i* playerCards+j];

                        var a= Cards[i + j];
                    }
                }
                List<int> s = new List<int>();

                for(int i=Cards.Count)
                s.AddRange


            }
            else
            {
                MessageBox.Show("請輸入人數");
            }
        }

        private void Shuffle()
        {
            Random random = new Random();

            for (int i = 0; i < Cards.Count; i++)
            {
                int j = random.Next(0, Cards.Count - 1);
                int tmp = Cards[i];
                Cards[i] = Cards[j];
                Cards[j] = tmp;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.Composition;

namespace MyWebCrawler
{
    [InheritedExport]
    public abstract class WebCrawler
    {
        public string Url { get; protected set; }

        public string TableName { get; protected set; }

        public string Data { get; protected set; }

        public string Path { get; protected set; }


        void Flow()
        {
            Download();
            Check();
            Save();
            ReadOriginalFile();
            SaveXml();
            ReadXmlFile();
            TranformToDbData();
            SaveDb();
        }

        public abstract bool Download();

        public abstract bool Check();

        public abstract bool Save();

        public abstract bool ReadOriginalFile();

        public abstract bool SaveXml();

        public abstract bool ReadXmlFile();

        public abstract bool TranformToDbData();

        public abstract bool SaveDb();
    }
}

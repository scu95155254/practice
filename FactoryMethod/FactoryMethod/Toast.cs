﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class Toast:IBread
    {
        public string Name { get;}
        

        public Toast(string name)
        {
            this.Name = name;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FactoryMethod
{
    class ToastMaker
    {
        public Toast CreateToast(string name)
        {
            return new Toast(name);
        }
    }
}

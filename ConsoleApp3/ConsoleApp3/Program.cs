﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            TypeConverter typeConverter = new TypeConverter();
            string a = "23";
            decimal b = 23;
         
            var d= changeType(b, a);
            bool wer = d.Equals(b);
            Console.WriteLine(wer);

            Console.WriteLine(d.GetType());
            
        }
  
        private static object changeType<T>(T b,string c)
        {
            
            return Convert.ChangeType(c, typeof(T));
            
         
        } 
    }
}

﻿using CMoney.Kernel;
using DataProc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_日期貨盤後行情表
{
    class SourceManager:IRunSource
    {
        private ISource SourceInfo;

        public SourceManager(ISource checkDataTable)
        {
            SourceInfo = checkDataTable;
     
        }

        public void SetSourceData(SourceInfo src)
        {

            foreach (string cycle in SourceInfo.Ps.DateRange)
            {
                if (DB.IsHoliday(cycle))
                {
                    continue;
                }
                //判斷是否有傳入樣本，如果沒有，代表全樣本
                List<string> sampleList = SourceInfo.IsAllSample ? src.GetResult(cycle, string.Empty).RowSamples : src.GetResult(cycle, string.Empty).RowSamples.Where(sample => SourceInfo.Ps.Samples.Contains(sample)).ToList();

                foreach (string sample in sampleList)
                {

                    //SetDataTable(cycle, sample, src);
                }
            }
        }
    }
}

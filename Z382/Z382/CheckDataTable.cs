﻿using CMoney.Kernel;
using DataProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_日期貨盤後行情表
{
    public class CheckDataTable:ISource
    {
        /// <summary>
        /// 這支程式碼的CTIME，統一建立時間
        /// </summary>
        public DateTime Ctime { get; }

        /// <summary>
        /// 這支程式碼的MTIME，統一修改時間
        /// </summary>
        public int Mtime { get; }

        public bool IsAllSample { get; }

        private string ConnectString { get; set; }

        public DataTable SourceTable { get; private set; }

        private DataTable NameTable { get; set; }

        public ProgramSetting Ps { get; }
        /// <summary>
        /// SQL欄位與MDB對應的欄位
        /// </summary>
        private Dictionary<string, string> ColumnChangeDic { get; set; }

        public CheckDataTable(ProgramSetting ps)
        {
            //這支程式碼的CTIME，統一建立時間
            Ctime = DateTime.Now;

            //這支程式碼的MTIME，統一修改時間
            Mtime = DB.GetMTime();

            //判斷是否為全樣本
            IsAllSample = (string.IsNullOrEmpty(ps.Samples.FirstOrDefault()) && ps.Samples.Count == 1) || (ps.Samples.Count == 0);

            //取SQL連線字串
            ConnectString = DB.GetSysDataSetting(Constant.上端程式, ps.SysDataDB, Constant.連線字串);

            Ps = ps;


            //比較除了PK跟CTIME、MTIME之外的欄位  SQL對應MDB
            SetDic();

    
        }

      

        /// <summary>
        /// 設定我要取得資料表的參數
        /// </summary>
        /// <param name="connStr">連線字串</param>
        /// <param name="PS">可以拿到週期樣本</param>
        /// <returns>資料表</returns>
        public void SetSourceTable()
        {
            IConnect connect = new ConnectSql();

            List<(string conditionColumn, IEnumerable<string> conditionValue)> condition = new List<(string conditionColumn, IEnumerable<string> conditionValue)>
            {
               (Constant.CYCLE, Ps.DateRange)
            };
            if (!IsAllSample)
            {
                condition.Add((Constant.SAMPLE, Ps.Samples));
            }
            SourceTable = connect.GetDataTable(true, Ps.ProgramName, "*", ConnectString, condition);
        }


        /// <summary>
        /// 設定我要取得名稱欄位的資料表
        /// </summary>
        /// <param name="connStr">連線字串</param>
        /// <param name="ps">取得週期跟樣本</param>
        /// <returns>資料表</returns>
        public void SetNameTable()
        {
            //把cycle轉換成year
            HashSet<string> years = new HashSet<string>();
            foreach (string cycle in Ps.DateRange)
            {
                years.Add(cycle.Substring(0, 4));
            }

            IConnect connect = new ConnectSql();

            List<(string conditionColumn, IEnumerable<string> conditionValue)> condition = new List<(string conditionColumn, IEnumerable<string> conditionValue)>
            {
               
                (Constant.YEAR,  years.Select(year=>$"N'{year}'"))
            };
            if (!IsAllSample)
            {
                condition.Add((Constant.交易所編碼, Ps.Samples));
            }

            NameTable = connect.GetDataTable(false, "期貨契約基本資料表", $"{Constant.YEAR},{Constant.交易所編碼},{Constant.NAME}", ConnectString, condition);

            NameTable.PrimaryKey = new DataColumn[] { NameTable.Columns[Constant.YEAR], NameTable.Columns[Constant.交易所編碼] };
        }


        /// <summary>
        /// 比較資料的流程
        /// </summary>
        /// <param name="ps">ProgramSetting</param>
        /// <param name="isAllSample">是否全樣本</param>
        /// <param name="nameTable">名稱表</param>
        /// <param name="sourceTable">SQL資料表</param>
        public void CompareDataFlow()
        {
            Dictionary<int,IRunSource> sources = new Dictionary<int,IRunSource>() { { 382101, new SourceManager(this) } };
            foreach (SourceInfo src in Ps.RunSourceInfo)
            {
                sources[src.SourceID].SetSourceData(src);
            }
        }

        /// <summary>
        /// 設定資料表
        /// </summary>
        /// <param name="cycle">週期列</param>
        /// <param name="sample">樣本列</param>
        /// <param name="src">設定表</param>
        /// <param name="sourceTable">資料庫的資料</param>
        /// <param name="nameTable">名稱的資料表</param>
        private void SetDataTable(string cycle, string sample, SourceInfo src)
        {
            ParseResult firstSample = src.GetResult(cycle, sample); //先用第一樣本取出MDB資料

            foreach (string secondSample in firstSample.RowSamples)
            {
                //MDB裡取回的資料結果，第二樣本
                ParseResult srcData = firstSample.GetSingleResult(secondSample); //使用GetSingleResult()取出資料

                //用PK找出SQL資料庫的這筆資料
                DataRow dataRow = SourceTable.Rows.Find(new object[] { cycle, sample, secondSample });

                //判斷資料庫有沒有這筆資料。如果沒有，取MDB新增至DataRow
                if (dataRow == null)
                {
                    //如果取回沒資料就設定PK，有資料就不變
                    SetPrimaryKey(ref dataRow, cycle, sample, secondSample);
                }

                //TODO:取得SQL的資料跟MDB的資料
                GetValue(dataRow, srcData, sample);

                if (dataRow.RowState != DataRowState.Unchanged)
                {
                    dataRow[Constant.MTIME] = Mtime;
                }
            }
        }

        /// <summary>
        /// 設定PK欄位
        /// </summary>
        /// <param name="dataRow">資料列</param>
        /// <param name="sourceTable">MDB來源</param>
        /// <param name="cycle">週期</param>
        /// <param name="sample">樣本</param>
        /// <param name="secondSample">第二樣本</param>
        private void SetPrimaryKey(ref DataRow dataRow, string cycle, string sample, string secondSample)
        {
            dataRow = SourceTable.NewRow();
            dataRow[Constant.CTIME] = Ctime;
            dataRow[Constant.CYCLE] = cycle;
            dataRow[Constant.SAMPLE] = sample;
            dataRow[Constant.SECSAMPLE] = secondSample;
            dataRow[Constant.OUTPUT_NAME] = $"{cycle}{sample}{Constant.PM}";
            SourceTable.Rows.Add(dataRow);
        }

        /// <summary>
        /// 比較的方法
        /// </summary>
        /// <param name="dataRow">dataRow</param>
        /// <param name="srcData">srcData</param>
        /// <param name="nameTable">名稱來源資料表</param>
        /// <param name="sample">sample</param>
        private void GetValue(DataRow dataRow, ParseResult srcData, string sample)
        {
            string name = NameTable.Rows.Find(new object[] { srcData.GetValue(Constant.交易日期).Substring(0, 4), sample }).Field<string>(Constant.NAME);
            //比對每個欄位
            foreach (KeyValuePair<string, string> columnSql in ColumnChangeDic)
            {
                //要比對的資料
                string sourceValue = string.Empty;
                switch (columnSql.Key)
                {
                    case Constant.NAME:
                        sourceValue = name;
                        break;
                    case Constant.OUTPUT_NAME:
                        sourceValue = $"{srcData.GetValue(Constant.契約)}{srcData.GetValue(Constant.到期月份)}PM";
                        break;
                    case Constant.OPEN_PRICE:
                    case Constant.MAX_PRICE:
                    case Constant.MIN_PIRCE:
                    case Constant.CLOSE_PRICE:
                    case Constant.UPDOWN:
                    case Constant.VOLUME:
                    case Constant.COUNT:

                        string columnvalue = srcData.GetValue(columnSql.Value);
                        if (!columnvalue.Equals("-"))
                        {
                            sourceValue = columnvalue;
                        }
                        break;
                }
                //比較SQL欄位跟MDB的欄位使否相同，不相同才進去做更新的動作
                var a = dataRow.Table.Columns[columnSql.Key].DataType;
                if (!Compare(dataRow[columnSql.Key], sourceValue, out object saveValue))
                {
                    //TODO:更新SQL資料
                    dataRow[columnSql.Key] = string.IsNullOrEmpty(sourceValue) ? (object)DBNull.Value : saveValue;
                }
            }
        }

        /// <summary>
        /// 兩個資料庫欄位的字典
        /// </summary>
        /// <returns>字典</returns>
        private void SetDic()
        {
            ColumnChangeDic = new Dictionary<string, string>
            {
                { Constant.NAME, string.Empty },
                { Constant.OPEN_PRICE, Constant.OPEN_PRICE },
                { Constant.MAX_PRICE, Constant.MAX_PRICE },
                { Constant.MIN_PIRCE, Constant.MIN_PIRCE },
                { Constant.CLOSE_PRICE, Constant.CLOSE_PRICE },
                { Constant.UPDOWN, Constant.漲跌價 },
                { Constant.VOLUME, Constant.VOLUME },
                { Constant.COUNT, Constant.COUNT },
                { Constant.OUTPUT_NAME, string.Empty }
            };
        }


        /// <summary>
        /// 比較的方法，判斷資料是否相同
        /// </summary>
        /// <typeparam name="T">SQL欄位的型態</typeparam>
        /// <param name="targetData">資料庫目標</param>
        /// <param name="sourceData">MDB來源</param>
        /// <param name="saveValue">要存進去DB的值</param>
        /// <returns>判斷值是否相同。相同True，不同False</returns>
        private bool Compare<T>(T targetData, string sourceData, out object saveValue)
        {
            bool isDBNull = targetData.GetType() == typeof(DBNull);
            bool isNull = string.IsNullOrEmpty(sourceData);
            saveValue = decimal.Round(decimal.Parse(sourceData),2,MidpointRounding.AwayFromZero);
            //如果兩邊都是空值
            if (isDBNull && isNull)
            {
                return true;
            }else if (!(isDBNull || isNull))
            {

                var sourceDataChangeType = Convert.ChangeType(sourceData, targetData.GetType());

                if (sourceDataChangeType.GetType() == typeof(decimal))
                {
                    int 小數點後的位數 = 1;
                    sourceDataChangeType = decimal.Round((decimal)sourceDataChangeType, targetData.ToString().Split('.')[小數點後的位數].Length, MidpointRounding.AwayFromZero);
                }



                saveValue = sourceDataChangeType;
                return targetData.Equals(sourceDataChangeType);
            }
            return false;
        }
    }
}

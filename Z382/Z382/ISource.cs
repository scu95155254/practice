﻿using DataProc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_日期貨盤後行情表
{
    interface ISource
    {
        ProgramSetting Ps { get; }

        DateTime Ctime { get; }

        int Mtime { get; }

        bool IsAllSample { get; }
    }
}

﻿using CMoney.Kernel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_日期貨盤後行情表
{
    /// <summary>
    /// 連線資料庫
    /// </summary>
    public class ConnectSql : IConnect
    {

        public DataTable GetDataTable(bool isContainSchema, string tableName, string columns, string connStr, List<(string conditionColumn, IEnumerable<string> conditionValue)> conditionSet)
        {
            string sqlCmr = $"SELECT {columns} FROM {tableName} ";

            if (conditionSet != null && conditionSet.Count() != 0)
            {
                sqlCmr = $"{sqlCmr} WHERE ";
                for (int i = 0; i < conditionSet.Count(); i++)
                {
                    //組合查詢條件語法 
                    sqlCmr = $"{sqlCmr} {string.Format("{0} in ({1})", conditionSet[i].conditionColumn, string.Join(",", conditionSet[i].conditionValue))} ";

                    //若i小於最後一個加AND
                    if (i < conditionSet.Count() - 1)
                    {
                        sqlCmr = $"{sqlCmr} AND";
                    }
                }
            }

            return isContainSchema ? DB.DoQuerySQLWithSchema(sqlCmr, connStr) : DB.DoQuerySQL(sqlCmr, connStr);
        }

    }
}

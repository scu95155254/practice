﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_日期貨盤後行情表
{
    public interface IConnect
    {
        DataTable GetDataTable(bool isContainSchema,string tableName,string columns,string connStr, List<(string conditionColumn, IEnumerable<string> conditionValue)> conditionSet);
    }
}

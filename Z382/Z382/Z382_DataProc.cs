﻿using CMoney.Kernel;
using DataProc;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Z382_日期貨盤後行情表
{
    /// <summary>
    /// 處理教學
    /// </summary>
    public class Z382_DataProc : IDataProc
    {
        /// <summary>
        /// 略過
        /// </summary>
        /// <param name="CheckList">略1</param>
        /// <param name="Cycle">略2</param>
        /// <param name="Sample">略3</param>
        /// <returns>略4</returns>
        public List<CheckResult> DataCheck(List<int> CheckList, string Cycle, string Sample)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 略過
        /// </summary>
        /// <param name="CheckList">略1</param>
        /// <param name="CheckData">略2</param>
        /// <returns>略3</returns>
        public List<CheckResult> DataCheck(List<int> CheckList, DataTable CheckData)
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// 處理實作
        /// </summary>
        /// <param name="ps">設定檔</param>
        /// <returns>資料結果</returns>
        public DataResult GetProc(ProgramSetting ps)
        {
            CheckDataTable checkDataTable = new CheckDataTable(ps);
            //取得資料庫要處理的資料表
            checkDataTable.SetSourceTable();
            //取得要設定名稱欄位的那張資料表，只需要年度、交易所編碼、名稱
            checkDataTable.SetNameTable();
            //SentMsg(ps);
            checkDataTable.CompareDataFlow();

            return new DataResult()
            {
                dtData = checkDataTable.SourceTable.GetChanges(), //設定處理後要更新回DB的DataTable
                Success = true//設定執行成功};
            };

        }

        /// <summary>
        /// 寄信的方法
        /// </summary>
        /// <param name="ps">ProgramSetting</param>
        private void SentMsg(ProgramSetting ps)
        {
            ProcessChangeEventArgs msg = new ProcessChangeEventArgs
            {
                Part = 5, //訊息等級
                ProcessMessage = "寄信測試，處理執行完成"
            };
            ps.SendMessage(msg);
        }
    }
}

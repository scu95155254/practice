﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_日期貨盤後行情表
{
    /// <summary>
    /// 常數
    /// </summary>
    public class Constant
    {
        /// <summary>
        /// 建立日期
        /// </summary>
        public const string CTIME = "CTIME";

        /// <summary>
        /// 週期
        /// </summary>
        public const string CYCLE = "日期";

        /// <summary>
        /// 樣本
        /// </summary>
        public const string SAMPLE = "代號";

        /// <summary>
        /// 第二樣本
        /// </summary>
        public const string SECSAMPLE = "交割月份";

        /// <summary>
        /// 代號+交割月份+PM
        /// </summary>
        public const string OUTPUT_NAME = "輸出代號";

        /// <summary>
        /// PM
        /// </summary>
        public const string PM = "PM";

        /// <summary>
        /// 修改時間
        /// </summary>
        public const string MTIME = "MTIME";

        /// <summary>
        /// 開盤價
        /// </summary>
        public const string OPEN_PRICE = "開盤價";

        /// <summary>
        /// 最高價
        /// </summary>
        public const string MAX_PRICE = "最高價";

        /// <summary>
        /// 最低價
        /// </summary>
        public const string MIN_PIRCE = "最低價";

        /// <summary>
        /// 收盤價
        /// </summary>
        public const string CLOSE_PRICE = "收盤價";

        /// <summary>
        /// 漲跌
        /// </summary>
        public const string UPDOWN = "漲跌";

        /// <summary>
        /// 成交量
        /// </summary>
        public const string VOLUME = "成交量";

        /// <summary>
        /// 名稱
        /// </summary>
        public const string NAME = "名稱";

        /// <summary>
        /// 未沖銷契約數
        /// </summary>
        public const string COUNT = "未沖銷契約數";

        /// <summary>
        /// 上端程式
        /// </summary>
        public const string 上端程式 = "上端程式";

        /// <summary>
        /// 連線字串
        /// </summary>
        public const string 連線字串 = "連線字串";

        /// <summary>
        /// 交易日期
        /// </summary>
        public const string 交易日期 = "交易日期";

        /// <summary>
        /// 漲跌價
        /// </summary>
        public const string 漲跌價 = "漲跌價";

        /// <summary>
        /// 交易所編碼
        /// </summary>釐清
        public const string 交易所編碼 = "交易所編碼";

        /// <summary>
        /// 年度
        /// </summary>
        public const string YEAR = "年度";

        /// <summary>
        /// 契約
        /// </summary>
        public const string 契約 = "契約";

        /// <summary>
        /// 到期月份(週別)
        /// </summary>
        public const string 到期月份 = "到期月份(週別)";
    }
}

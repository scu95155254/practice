﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Z382_DownloadExtension
{
    public class RegexWebSourceData : IRegex
    {
        public Dictionary<string, List<string>> RegexParam(string sourceData, string pattern, List<string> groupName)
        {
            Dictionary<string, List<string>> totalRow = new Dictionary<string, List<string>>();

            foreach (string name in groupName)
            {
                List<string> row = new List<string>();
                foreach (Match match in Regex.Matches(sourceData, pattern))
                {
                    if (match.Groups[name].Value.Equals(string.Empty))
                    {
                        continue;
                    }
                    row.Add(HttpUtility.UrlEncode( match.Groups[name].Value));
                }
                totalRow.Add(name, row);
            }
            return totalRow;
        }
    }
}

﻿using DownloadSystem.DataLibs.Interface;
using DownloadSystem.DataLibs.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_DownloadExtension
{
    [Export(typeof(ITaskExtension))]
    [ExportMetadata("Name", "Z382_DownloadExtension.SourceID_382106")]//TODO:修改為TaskInfo.ExtensionName
    [ExportMetadata("ContainsDatabaseOperation", false)]//TODO:註明是否包含讀取資料庫內容的操作
    [ExportMetadata("ContainsInternetOperation", false)]//TODO:註明是否有額外存取網路資料的操作
    class SourceID_382106 : ITaskExtension
    {
        public bool CheckContent(ref TaskInfo taskInfo, ref List<WebSourceData> downloadedWebSourceDataList, out List<WebSourceData> failedList)
        {
            throw new NotImplementedException();
        }

        public List<WebSourceData> GetList(List<WebSourceData> webSourceDataPrototypeList, string cycleRange, string samples, string extraParameter, List<WebSourceData> parentList = null)
        {

            List<WebSourceData> sonWebSourceData = new List<WebSourceData>();
            foreach (WebSourceData parentWeb in parentList)
            {
                //取得WebSource設定
                WebSourceData baseWebData = new WebSourceData(webSourceDataPrototypeList.FirstOrDefault());
                string htmlData = Encoding.GetEncoding(parentWeb.EncodingName).GetString(parentWeb.WebContent);
                IRegex regex = new RegexWebSourceData();
                Dictionary<string, List<string>> groupNameParams = regex.RegexParam(htmlData, @"name=""uniqueToken""\s*value=""(?<uniqueToken>.*)"" \/><input|name=""_rendertime_""\svalue=""(?<_rendertime_>.*)"" \/>|javax.faces.ViewState"" value=""(?<ViewState>.*)""||3"" type = ""radio"" name = ""queryType"" value = ""(?<queryType>.*)"" onkeypress = ""this.checked='checked';null"" > T", new List<string>() { "uniqueToken", "_rendertime_", "ViewState", "queryType" });
                baseWebData.PostData = string.Format(baseWebData.PostData, groupNameParams["uniqueToken"][0], groupNameParams["_rendertime_"][0],groupNameParams["queryType"][0], groupNameParams["ViewState"][0]);
            }

            return sonWebSourceData;
        }
    }
}

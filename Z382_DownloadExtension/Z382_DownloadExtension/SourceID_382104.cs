﻿using DownloadSystem.DataLibs.Interface;
using DownloadSystem.DataLibs.Models;
using MailForHandler;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Z382_DownloadExtension
{
    /// <summary>
    /// 下載處理的擴充
    /// </summary>
    [Export(typeof(ITaskExtension))]
    [ExportMetadata("Name", "Z382_DownloadExtension.SourceID_382104001")]//TODO:修改為TaskInfo.ExtensionName
    [ExportMetadata("ContainsDatabaseOperation", false)]//TODO:註明是否包含讀取資料庫內容的操作
    [ExportMetadata("ContainsInternetOperation", false)]//TODO:註明是否有額外存取網路資料的操作
    public class SourceID_382104 : ITaskExtension
    {
        /// <summary>
        /// 排除錯誤下載結果
        /// </summary>
        /// <param name="taskInfo">任務資訊</param>
        /// <param name="downloadedWebSourceDataList">要下載的資料</param>
        /// <param name="failedList">錯誤的資料來源</param>
        /// <returns>判斷是否需要下載</returns>
        public bool CheckContent(ref TaskInfo taskInfo, ref List<WebSourceData> downloadedWebSourceDataList, out List<WebSourceData> failedList)
        {
            failedList = new List<WebSourceData>();

            List<WebSourceData> faildDatas = downloadedWebSourceDataList.Where(x => x.WebContent == null || x.WebContent.Length == 0 || Encoding.GetEncoding(x.EncodingName).GetString(x.WebContent).Contains("<title></title>")).ToList();

            downloadedWebSourceDataList.RemoveAll(x => faildDatas.Select(y => y.URI).Contains(x.URI));

            failedList = faildDatas;

            MailInfo mailInfo = new MailInfo("Z382");
            mailInfo.sendMail($"{taskInfo.ID} Hank_Huang 測試寄信用", "測試信寄送成功", "請求參數設置錯誤");

            return failedList.Count == 0;
        }

        /// <summary>
        /// 解析母任務的結果，建立新的下載資料
        /// </summary>
        /// <param name="webSourceDataPrototypeList">初始的來源</param>
        /// <param name="cycleRange">傳入下載週期</param>
        /// <param name="samples">傳入下載樣本</param>
        /// <param name="extraParameter">額外的參數</param>
        /// <param name="parentList">母任務下載結果</param>
        /// <returns>要下載的資料</returns>
        public List<WebSourceData> GetList(List<WebSourceData> webSourceDataPrototypeList, string cycleRange, string samples, string extraParameter, List<WebSourceData> parentList = null)
        {
            List<WebSourceData> sonWebSourceData = new List<WebSourceData>();

            foreach (WebSourceData parentWeb in parentList)
            {
                //取得WebSource設定
                WebSourceData baseWebData = new WebSourceData(webSourceDataPrototypeList.FirstOrDefault());

                string htmlData = Encoding.GetEncoding(parentWeb.EncodingName).GetString(parentWeb.WebContent);

                IRegex regex = new RegexWebSourceData();
                //撈出我要的參數
                Dictionary<string, List<string>> groupNameParams = regex.RegexParam(htmlData, @"(id=""__EVENTVALIDATION"".*value=""(?<eventvalidation>.*)"")|(id=""__VIEWSTATE"".*value=""(?<viewstate>.*?)"")|(id=""__VIEWSTATEGENERATOR""\svalue=""(?<viewstategenerator>.*)"")", new List<string>() { "viewstate", "viewstategenerator", "eventvalidation" });

                baseWebData.Cycle = parentWeb.Cycle;

                baseWebData.PostData = string.Format(baseWebData.PostData, groupNameParams["viewstate"][0], groupNameParams["viewstategenerator"][0], groupNameParams["eventvalidation"][0], baseWebData.Cycle);

                sonWebSourceData.Add(baseWebData);
            }
            //回傳下載資料集合給下載器處理
            return sonWebSourceData;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_DownloadExtension
{
    public interface IRegex
    {
        Dictionary<string, List<string>> RegexParam(string sourceData, string pattern, List<string> groupName);
    }
}

﻿using DownloadSystem.DataLibs.Interface;
using DownloadSystem.DataLibs.Models;
using MailForHandler;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Z382_DownloadExtension
{
    [Export(typeof(ITaskExtension))]
    [ExportMetadata("Name", "Z382_DownloadExtension.SourceID_382105")]//TODO:修改為TaskInfo.ExtensionName
    [ExportMetadata("ContainsDatabaseOperation", false)]//TODO:註明是否包含讀取資料庫內容的操作
    [ExportMetadata("ContainsInternetOperation", false)]//TODO:註明是否有額外存取網路資料的操作
    public class SourceID_382105 : ITaskExtension
    {
        public bool CheckContent(ref TaskInfo taskInfo, ref List<WebSourceData> downloadedWebSourceDataList, out List<WebSourceData> failedList)
        {
            failedList = new List<WebSourceData>();

            List<WebSourceData> faildDatas = downloadedWebSourceDataList.Where(x => x.WebContent == null || x.WebContent.Length == 0 || Encoding.GetEncoding(x.EncodingName).GetString(x.WebContent).Contains(@"<table id=""meetinfo"" class=""c - cardTable"">")).ToList();

            downloadedWebSourceDataList.RemoveAll(x => faildDatas.Select(y => y.URI).Contains(x.URI));

            failedList = faildDatas;

            //MailInfo mailInfo = new MailInfo("Z382");
            //mailInfo.sendMail($"{taskInfo.ID} Hank_Huang 測試寄信用", "測試信寄送成功", "請求參數設置錯誤");

            return failedList.Count == 0;
        }

        public List<WebSourceData> GetList(List<WebSourceData> webSourceDataPrototypeList, string cycleRange, string samples, string extraParameter, List<WebSourceData> parentList = null)
        {
            List<WebSourceData> sonWebSourceData = new List<WebSourceData>();

            foreach(WebSourceData parentWeb in parentList)
            {

                

                string htmlData = Encoding.GetEncoding(parentWeb.EncodingName).GetString(parentWeb.WebContent);

                IRegex regex = new RegexWebSourceData();

                string totalPage=regex.RegexParam(htmlData, "頁次.\\d\\/(?<totalPage>\\d*)<td>", new List<string>() { "totalPage" })["totalPage"].First();

                for(int page = 1; page <= int.Parse(totalPage); page++)
                {
                    //取得WebSource設定
                    WebSourceData baseWebData = new WebSourceData(webSourceDataPrototypeList.FirstOrDefault());
                    baseWebData.Cycle = DateTime.Now.ToString("yyyyMMdd");
                    baseWebData.Sample = page.ToString();
                    baseWebData.URI = string.Format(baseWebData.URI, page.ToString());
                    sonWebSourceData.Add(baseWebData);
                }
                
            }
        

            return sonWebSourceData;
        }
    }
}

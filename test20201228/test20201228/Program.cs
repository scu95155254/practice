﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace test20201228
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "https://www.ezmoney.com.tw/ETF";

            HtmlWeb web = new HtmlWeb();
            HtmlDocument doc = web.Load(url);

            HtmlNodeCollection nodes = doc.DocumentNode.SelectNodes("//a");

            foreach (HtmlNode node in nodes)
            {
                Console.WriteLine(node.OuterHtml);
            }

            Console.Read();

        }
    }
}

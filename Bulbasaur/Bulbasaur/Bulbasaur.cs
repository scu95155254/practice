﻿using PokamonData;
using System.ComponentModel.Composition;

namespace Bulbasaur
{
    [Export("妙蛙種子")]
    public class Bulbasaur : IPokamonData
    {
        public string Name
        {
            get
            {
                return "妙蛙種子";
            }
        }

        public string Id
        {
            get
            {
                return "001";
            }
        }


        public string Attributes
        {
            get
            {
                return "草 & 毒";
            }
        }

        public string Skill
        {
            get
            {
                return "飛葉快刀";
            }
            set
            {
                Skill = "火焰球";
            }

        }
    }
}

﻿using System.ComponentModel.Composition;


namespace PokamonData
{
    [InheritedExport]
    public interface IPokamonData
    {
        string Name { get; }

        string Id { get; }
        
        string Attributes { get; }

        string Skill { get; set; }
    }
}

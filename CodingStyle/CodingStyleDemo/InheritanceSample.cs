﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class BaseSample
    {
    }

    internal class DerivedSample : BaseSample
    {
        internal int Id { get; set; }
    }
}

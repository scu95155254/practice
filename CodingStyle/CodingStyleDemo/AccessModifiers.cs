﻿using CodingStyleDemoInternal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    class AccessModifiers
    {
        #region 所有類別以及類別成員都必須標註
        //要能掌握自己程式碼的存取範圍
        //不同程式語言的預設是不同的 
        #endregion

        //官方介紹
        //https://docs.microsoft.com/zh-tw/dotnet/csharp/programming-guide/classes-and-structs/access-modifiers

        //internal
        AnotherNameSpace anotherNameSpace = new AnotherNameSpace();
        string a = "0";
       
    }
  

}


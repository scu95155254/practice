﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    //class FruitShop
    //{
    //    class Apple
    //    {
    //        internal void Packege()
    //        {

    //        }
    //    }
    //    void Sell()
    //    {
    //        Apple apple = new Apple();
    //        apple.Packege();
    //    }
    //}

    //class FruitShop
    //{
    //    class Apple
    //    {
    //        internal void Packege()
    //        {

    //        }
    //    }

    //    class Banana
    //    {
    //        internal void Packege()
    //        {

    //        }
    //    }
    //    void Sell()
    //    {
    //        Apple apple = new Apple();
    //        apple.Packege();
    //        Banana banana = new Banana();
    //        banana.Packege();
    //    }
    //}

    //class FruitShop
    //{
    //    class Apple
    //    {
    //        internal void Packege()
    //        {

    //        }
    //    }

    //    class Banana
    //    {
    //        internal void Packege()
    //        {

    //        }
    //    }

    //    class Orange
    //    {
    //        internal void Packege()
    //        {

    //        }
    //    }
    //    void Sell()
    //    {
    //        Apple apple = new Apple();
    //        apple.Packege();
    //        Banana banana = new Banana();
    //        banana.Packege();
    //        Orange orange = new Orange();
    //        orange.Packege();
    //    }
    //}

    class FruitShop
    {
        class Apple
        {
            internal void Packege()
            {

            }
        }

        class Banana
        {
            internal void Packege()
            {

            }
        }

        class Orange
        {
            internal void Packege()
            {

            }
        }

        class PackegeMaker
        {
            internal void Packege<T>()
            {
                //T.Packege();
            }
        }

        void Sell()
        {
            Apple apple = new Apple();
            apple.Packege();
            Banana banana = new Banana();
            banana.Packege();
            Orange orange = new Orange();
            orange.Packege();
        }
    }


    class Fruit
    {

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class ValueTupleDemo
    {
        internal static Tuple<int, string> GetDate()
        {
            return new Tuple<int, string>(1,"1");
        }
        //internal static(int , string ) GetDateValueTuple()
        //{
        //    int sum = 1;
        //    string name = "1";
        //    return (sum, name);
        //}
    }
}

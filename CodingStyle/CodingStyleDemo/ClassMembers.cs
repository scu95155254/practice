﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    class ClassMembers
    {
        //欄位
        int Field;

        /// <summary>
        /// 屬性
        /// </summary> 
        int Property { get; set; }

        /// <summary>
        /// 常數
        /// </summary>
        const int CONST = 0;

        /// <summary>
        /// 方法
        /// </summary>
        void GetData()
        {

        }

        /// <summary>
        /// 建構子
        /// </summary>
        internal ClassMembers()
        {

        }


        /// <summary>
        /// 店鋪(巢狀類別)
        /// </summary>
        internal class Shop
        {
            //蛋糕折扣
            internal double CakeDiscountRate;

            /// <summary>
            /// 餅乾折扣
            /// </summary>
            internal double CookieDiscountRate { get;private set; }

            internal Shop()
            {
                CakeDiscountRate = 0.8;
                CookieDiscountRate = 0.6;
            }

            //對外只能用屬性
            //沒有必要公開的屬性就不該公開
        }

        //事件、解構子
    }
}

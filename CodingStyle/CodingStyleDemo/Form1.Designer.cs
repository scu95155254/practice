﻿namespace CodingStyleDemo
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置受控資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonPurpose = new System.Windows.Forms.Button();
            this.ButtonClassMembers = new System.Windows.Forms.Button();
            this.ButtonAccessModifiers = new System.Windows.Forms.Button();
            this.ButtonFieldProperty = new System.Windows.Forms.Button();
            this.ButtonTaskList = new System.Windows.Forms.Button();
            this.ButtonCommentOut = new System.Windows.Forms.Button();
            this.ButtonName = new System.Windows.Forms.Button();
            this.ButtonDeliveryPrinciple = new System.Windows.Forms.Button();
            this.ButtonCSharpType = new System.Windows.Forms.Button();
            this.ButtonNewlineSeparate = new System.Windows.Forms.Button();
            this.ButtonDeclareVariable = new System.Windows.Forms.Button();
            this.ButtonDanglingElse = new System.Windows.Forms.Button();
            this.ButtonCurlyBrackets = new System.Windows.Forms.Button();
            this.ButtonGlobalItem = new System.Windows.Forms.Button();
            this.ButtonClassLocationRule = new System.Windows.Forms.Button();
            this.ButtonDuplicateCode = new System.Windows.Forms.Button();
            this.ButtonMagicNumber = new System.Windows.Forms.Button();
            this.ButtonStrongTypeLanguage = new System.Windows.Forms.Button();
            this.ButtonValueReferenceType = new System.Windows.Forms.Button();
            this.MessageRichBox = new System.Windows.Forms.RichTextBox();
            this.ButtonNullableType = new System.Windows.Forms.Button();
            this.ButtonDoubleSpecialValue = new System.Windows.Forms.Button();
            this.ButtonDynamic = new System.Windows.Forms.Button();
            this.ButtonVar = new System.Windows.Forms.Button();
            this.ButtonAnonymousType = new System.Windows.Forms.Button();
            this.ButtonRepeatCall = new System.Windows.Forms.Button();
            this.ButtonLocalFunctions = new System.Windows.Forms.Button();
            this.ButtonParams = new System.Windows.Forms.Button();
            this.ButtonCheckParameter = new System.Windows.Forms.Button();
            this.ButtonValueTuple = new System.Windows.Forms.Button();
            this.ButtonRef = new System.Windows.Forms.Button();
            this.ButtonOverloading = new System.Windows.Forms.Button();
            this.ButtonCallByReferenceValue = new System.Windows.Forms.Button();
            this.ButtonImplicitConversions = new System.Windows.Forms.Button();
            this.ButtonConversionRightWay = new System.Windows.Forms.Button();
            this.ButtonBoxingUnboxing = new System.Windows.Forms.Button();
            this.ButtonExplicitConversions = new System.Windows.Forms.Button();
            this.ButtonMethodLength = new System.Windows.Forms.Button();
            this.ButtonStringImmutable = new System.Windows.Forms.Button();
            this.ButtonStringSearch = new System.Windows.Forms.Button();
            this.ButtonStringEmpty = new System.Windows.Forms.Button();
            this.ButtonStringCompare = new System.Windows.Forms.Button();
            this.ButtonAt = new System.Windows.Forms.Button();
            this.ButtonStringConcat = new System.Windows.Forms.Button();
            this.ButtonMemoryAllocate = new System.Windows.Forms.Button();
            this.ButtonStatic = new System.Windows.Forms.Button();
            this.ButtonThreadSafe = new System.Windows.Forms.Button();
            this.ButtonGoto = new System.Windows.Forms.Button();
            this.ButtonDoEvents = new System.Windows.Forms.Button();
            this.ButtonGC = new System.Windows.Forms.Button();
            this.ButtonRaceCondition = new System.Windows.Forms.Button();
            this.ButtonFloatingPointNumber = new System.Windows.Forms.Button();
            this.ButtonDelegateAndEvent = new System.Windows.Forms.Button();
            this.ButtonDispose = new System.Windows.Forms.Button();
            this.ButtonException = new System.Windows.Forms.Button();
            this.ButtonExtendMethod = new System.Windows.Forms.Button();
            this.ButtonGenerics = new System.Windows.Forms.Button();
            this.ButtonOptionParameter = new System.Windows.Forms.Button();
            this.ButtonCSharp = new System.Windows.Forms.Button();
            this.buttonSolution = new System.Windows.Forms.Button();
            this.buttonObjectOriented = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButtonPurpose
            // 
            this.ButtonPurpose.Location = new System.Drawing.Point(133, 41);
            this.ButtonPurpose.Name = "ButtonPurpose";
            this.ButtonPurpose.Size = new System.Drawing.Size(124, 23);
            this.ButtonPurpose.TabIndex = 0;
            this.ButtonPurpose.Text = "目的";
            this.ButtonPurpose.UseVisualStyleBackColor = true;
            this.ButtonPurpose.Click += new System.EventHandler(this.ButtonPurpose_Click);
            // 
            // ButtonClassMembers
            // 
            this.ButtonClassMembers.Location = new System.Drawing.Point(133, 340);
            this.ButtonClassMembers.Name = "ButtonClassMembers";
            this.ButtonClassMembers.Size = new System.Drawing.Size(124, 23);
            this.ButtonClassMembers.TabIndex = 1;
            this.ButtonClassMembers.Text = "類別成員";
            this.ButtonClassMembers.UseVisualStyleBackColor = true;
            this.ButtonClassMembers.Click += new System.EventHandler(this.ButtonClassMembers_Click);
            // 
            // ButtonAccessModifiers
            // 
            this.ButtonAccessModifiers.Location = new System.Drawing.Point(133, 384);
            this.ButtonAccessModifiers.Name = "ButtonAccessModifiers";
            this.ButtonAccessModifiers.Size = new System.Drawing.Size(124, 23);
            this.ButtonAccessModifiers.TabIndex = 2;
            this.ButtonAccessModifiers.Text = "存取修飾詞";
            this.ButtonAccessModifiers.UseVisualStyleBackColor = true;
            this.ButtonAccessModifiers.Click += new System.EventHandler(this.ButtonAccessModifiers_Click);
            // 
            // ButtonFieldProperty
            // 
            this.ButtonFieldProperty.Location = new System.Drawing.Point(133, 423);
            this.ButtonFieldProperty.Name = "ButtonFieldProperty";
            this.ButtonFieldProperty.Size = new System.Drawing.Size(124, 23);
            this.ButtonFieldProperty.TabIndex = 3;
            this.ButtonFieldProperty.Text = "欄位與屬性";
            this.ButtonFieldProperty.UseVisualStyleBackColor = true;
            this.ButtonFieldProperty.Click += new System.EventHandler(this.ButtonFieldProperty_Click);
            // 
            // ButtonTaskList
            // 
            this.ButtonTaskList.Location = new System.Drawing.Point(270, 471);
            this.ButtonTaskList.Name = "ButtonTaskList";
            this.ButtonTaskList.Size = new System.Drawing.Size(124, 23);
            this.ButtonTaskList.TabIndex = 10;
            this.ButtonTaskList.Text = "工作清單";
            this.ButtonTaskList.UseVisualStyleBackColor = true;
            this.ButtonTaskList.Click += new System.EventHandler(this.ButtonTaskList_Click);
            // 
            // ButtonCommentOut
            // 
            this.ButtonCommentOut.Location = new System.Drawing.Point(270, 384);
            this.ButtonCommentOut.Name = "ButtonCommentOut";
            this.ButtonCommentOut.Size = new System.Drawing.Size(124, 23);
            this.ButtonCommentOut.TabIndex = 11;
            this.ButtonCommentOut.Text = "註解";
            this.ButtonCommentOut.UseVisualStyleBackColor = true;
            this.ButtonCommentOut.Click += new System.EventHandler(this.ButtonCommentOut_Click);
            // 
            // ButtonName
            // 
            this.ButtonName.Location = new System.Drawing.Point(270, 340);
            this.ButtonName.Name = "ButtonName";
            this.ButtonName.Size = new System.Drawing.Size(124, 23);
            this.ButtonName.TabIndex = 12;
            this.ButtonName.Text = "命名";
            this.ButtonName.UseVisualStyleBackColor = true;
            this.ButtonName.Click += new System.EventHandler(this.ButtonName_Click);
            // 
            // ButtonDeliveryPrinciple
            // 
            this.ButtonDeliveryPrinciple.Location = new System.Drawing.Point(270, 510);
            this.ButtonDeliveryPrinciple.Name = "ButtonDeliveryPrinciple";
            this.ButtonDeliveryPrinciple.Size = new System.Drawing.Size(124, 23);
            this.ButtonDeliveryPrinciple.TabIndex = 13;
            this.ButtonDeliveryPrinciple.Text = "交付原則";
            this.ButtonDeliveryPrinciple.UseVisualStyleBackColor = true;
            this.ButtonDeliveryPrinciple.Click += new System.EventHandler(this.ButtonDeliveryPrinciple_Click);
            // 
            // ButtonCSharpType
            // 
            this.ButtonCSharpType.Location = new System.Drawing.Point(133, 211);
            this.ButtonCSharpType.Name = "ButtonCSharpType";
            this.ButtonCSharpType.Size = new System.Drawing.Size(124, 23);
            this.ButtonCSharpType.TabIndex = 14;
            this.ButtonCSharpType.Text = "C#內置型別";
            this.ButtonCSharpType.UseVisualStyleBackColor = true;
            this.ButtonCSharpType.Click += new System.EventHandler(this.ButtonCSharpType_Click);
            // 
            // ButtonNewlineSeparate
            // 
            this.ButtonNewlineSeparate.Location = new System.Drawing.Point(270, 294);
            this.ButtonNewlineSeparate.Name = "ButtonNewlineSeparate";
            this.ButtonNewlineSeparate.Size = new System.Drawing.Size(124, 23);
            this.ButtonNewlineSeparate.TabIndex = 15;
            this.ButtonNewlineSeparate.Text = "換行分隔";
            this.ButtonNewlineSeparate.UseVisualStyleBackColor = true;
            this.ButtonNewlineSeparate.Click += new System.EventHandler(this.ButtonNewlineSeparate_Click);
            // 
            // ButtonDeclareVariable
            // 
            this.ButtonDeclareVariable.Location = new System.Drawing.Point(270, 250);
            this.ButtonDeclareVariable.Name = "ButtonDeclareVariable";
            this.ButtonDeclareVariable.Size = new System.Drawing.Size(124, 23);
            this.ButtonDeclareVariable.TabIndex = 16;
            this.ButtonDeclareVariable.Text = "宣告變數";
            this.ButtonDeclareVariable.UseVisualStyleBackColor = true;
            this.ButtonDeclareVariable.Click += new System.EventHandler(this.ButtonDeclareVariable_Click);
            // 
            // ButtonDanglingElse
            // 
            this.ButtonDanglingElse.Location = new System.Drawing.Point(270, 211);
            this.ButtonDanglingElse.Name = "ButtonDanglingElse";
            this.ButtonDanglingElse.Size = new System.Drawing.Size(124, 23);
            this.ButtonDanglingElse.TabIndex = 17;
            this.ButtonDanglingElse.Text = "DanglingElse";
            this.ButtonDanglingElse.UseVisualStyleBackColor = true;
            this.ButtonDanglingElse.Click += new System.EventHandler(this.ButtonDanglingElse_Click);
            // 
            // ButtonCurlyBrackets
            // 
            this.ButtonCurlyBrackets.Location = new System.Drawing.Point(270, 172);
            this.ButtonCurlyBrackets.Name = "ButtonCurlyBrackets";
            this.ButtonCurlyBrackets.Size = new System.Drawing.Size(124, 23);
            this.ButtonCurlyBrackets.TabIndex = 18;
            this.ButtonCurlyBrackets.Text = "大括號使用";
            this.ButtonCurlyBrackets.UseVisualStyleBackColor = true;
            this.ButtonCurlyBrackets.Click += new System.EventHandler(this.ButtonCurlyBrackets_Click);
            // 
            // ButtonGlobalItem
            // 
            this.ButtonGlobalItem.Location = new System.Drawing.Point(270, 129);
            this.ButtonGlobalItem.Name = "ButtonGlobalItem";
            this.ButtonGlobalItem.Size = new System.Drawing.Size(124, 23);
            this.ButtonGlobalItem.TabIndex = 19;
            this.ButtonGlobalItem.Text = "共用項目";
            this.ButtonGlobalItem.UseVisualStyleBackColor = true;
            this.ButtonGlobalItem.Click += new System.EventHandler(this.ButtonGlobalItem_Click);
            // 
            // ButtonClassLocationRule
            // 
            this.ButtonClassLocationRule.Location = new System.Drawing.Point(133, 294);
            this.ButtonClassLocationRule.Name = "ButtonClassLocationRule";
            this.ButtonClassLocationRule.Size = new System.Drawing.Size(124, 23);
            this.ButtonClassLocationRule.TabIndex = 20;
            this.ButtonClassLocationRule.Text = "類別存放規則";
            this.ButtonClassLocationRule.UseVisualStyleBackColor = true;
            this.ButtonClassLocationRule.Click += new System.EventHandler(this.ButtonClassLocationRule_Click);
            // 
            // ButtonDuplicateCode
            // 
            this.ButtonDuplicateCode.Location = new System.Drawing.Point(270, 85);
            this.ButtonDuplicateCode.Name = "ButtonDuplicateCode";
            this.ButtonDuplicateCode.Size = new System.Drawing.Size(124, 23);
            this.ButtonDuplicateCode.TabIndex = 21;
            this.ButtonDuplicateCode.Text = "重複程式碼";
            this.ButtonDuplicateCode.UseVisualStyleBackColor = true;
            this.ButtonDuplicateCode.Click += new System.EventHandler(this.ButtonDuplicateCode_Click);
            // 
            // ButtonMagicNumber
            // 
            this.ButtonMagicNumber.Location = new System.Drawing.Point(270, 41);
            this.ButtonMagicNumber.Name = "ButtonMagicNumber";
            this.ButtonMagicNumber.Size = new System.Drawing.Size(124, 23);
            this.ButtonMagicNumber.TabIndex = 22;
            this.ButtonMagicNumber.Text = "MagicNumber";
            this.ButtonMagicNumber.UseVisualStyleBackColor = true;
            this.ButtonMagicNumber.Click += new System.EventHandler(this.ButtonMagicNumber_Click);
            // 
            // ButtonStrongTypeLanguage
            // 
            this.ButtonStrongTypeLanguage.Location = new System.Drawing.Point(133, 170);
            this.ButtonStrongTypeLanguage.Name = "ButtonStrongTypeLanguage";
            this.ButtonStrongTypeLanguage.Size = new System.Drawing.Size(124, 23);
            this.ButtonStrongTypeLanguage.TabIndex = 25;
            this.ButtonStrongTypeLanguage.Text = "強型別介紹";
            this.ButtonStrongTypeLanguage.UseVisualStyleBackColor = true;
            this.ButtonStrongTypeLanguage.Click += new System.EventHandler(this.ButtonStrongTypeLanguage_Click);
            // 
            // ButtonValueReferenceType
            // 
            this.ButtonValueReferenceType.Location = new System.Drawing.Point(419, 41);
            this.ButtonValueReferenceType.Name = "ButtonValueReferenceType";
            this.ButtonValueReferenceType.Size = new System.Drawing.Size(124, 23);
            this.ButtonValueReferenceType.TabIndex = 24;
            this.ButtonValueReferenceType.Text = "值型別參考型別";
            this.ButtonValueReferenceType.UseVisualStyleBackColor = true;
            this.ButtonValueReferenceType.Click += new System.EventHandler(this.ButtonValueReferenceType_Click);
            // 
            // MessageRichBox
            // 
            this.MessageRichBox.Location = new System.Drawing.Point(12, 12);
            this.MessageRichBox.Name = "MessageRichBox";
            this.MessageRichBox.Size = new System.Drawing.Size(115, 722);
            this.MessageRichBox.TabIndex = 26;
            this.MessageRichBox.Text = "";
            // 
            // ButtonNullableType
            // 
            this.ButtonNullableType.Location = new System.Drawing.Point(549, 89);
            this.ButtonNullableType.Name = "ButtonNullableType";
            this.ButtonNullableType.Size = new System.Drawing.Size(124, 23);
            this.ButtonNullableType.TabIndex = 28;
            this.ButtonNullableType.Text = "Nullable Type";
            this.ButtonNullableType.UseVisualStyleBackColor = true;
            this.ButtonNullableType.Click += new System.EventHandler(this.ButtonNullableType_Click);
            // 
            // ButtonDoubleSpecialValue
            // 
            this.ButtonDoubleSpecialValue.Location = new System.Drawing.Point(549, 172);
            this.ButtonDoubleSpecialValue.Name = "ButtonDoubleSpecialValue";
            this.ButtonDoubleSpecialValue.Size = new System.Drawing.Size(124, 23);
            this.ButtonDoubleSpecialValue.TabIndex = 27;
            this.ButtonDoubleSpecialValue.Text = "Double特殊值";
            this.ButtonDoubleSpecialValue.UseVisualStyleBackColor = true;
            this.ButtonDoubleSpecialValue.Click += new System.EventHandler(this.ButtonDoubleSpecialValue_Click);
            // 
            // ButtonDynamic
            // 
            this.ButtonDynamic.Location = new System.Drawing.Point(549, 364);
            this.ButtonDynamic.Name = "ButtonDynamic";
            this.ButtonDynamic.Size = new System.Drawing.Size(124, 23);
            this.ButtonDynamic.TabIndex = 32;
            this.ButtonDynamic.Text = "動態型別dynamic";
            this.ButtonDynamic.UseVisualStyleBackColor = true;
            this.ButtonDynamic.Click += new System.EventHandler(this.ButtonDynamic_Click);
            // 
            // ButtonVar
            // 
            this.ButtonVar.Location = new System.Drawing.Point(549, 250);
            this.ButtonVar.Name = "ButtonVar";
            this.ButtonVar.Size = new System.Drawing.Size(124, 23);
            this.ButtonVar.TabIndex = 30;
            this.ButtonVar.Text = "隱含型別";
            this.ButtonVar.UseVisualStyleBackColor = true;
            this.ButtonVar.Click += new System.EventHandler(this.ButtonVar_Click);
            // 
            // ButtonAnonymousType
            // 
            this.ButtonAnonymousType.Location = new System.Drawing.Point(549, 211);
            this.ButtonAnonymousType.Name = "ButtonAnonymousType";
            this.ButtonAnonymousType.Size = new System.Drawing.Size(124, 23);
            this.ButtonAnonymousType.TabIndex = 29;
            this.ButtonAnonymousType.Text = "匿名型別";
            this.ButtonAnonymousType.UseVisualStyleBackColor = true;
            this.ButtonAnonymousType.Click += new System.EventHandler(this.ButtonAnonymousType_Click);
            // 
            // ButtonRepeatCall
            // 
            this.ButtonRepeatCall.Location = new System.Drawing.Point(687, 172);
            this.ButtonRepeatCall.Name = "ButtonRepeatCall";
            this.ButtonRepeatCall.Size = new System.Drawing.Size(124, 23);
            this.ButtonRepeatCall.TabIndex = 40;
            this.ButtonRepeatCall.Text = "呼叫";
            this.ButtonRepeatCall.UseVisualStyleBackColor = true;
            this.ButtonRepeatCall.Click += new System.EventHandler(this.ButtonRepeatCall_Click);
            // 
            // ButtonLocalFunctions
            // 
            this.ButtonLocalFunctions.Location = new System.Drawing.Point(687, 250);
            this.ButtonLocalFunctions.Name = "ButtonLocalFunctions";
            this.ButtonLocalFunctions.Size = new System.Drawing.Size(124, 23);
            this.ButtonLocalFunctions.TabIndex = 39;
            this.ButtonLocalFunctions.Text = "區域函式";
            this.ButtonLocalFunctions.UseVisualStyleBackColor = true;
            this.ButtonLocalFunctions.Click += new System.EventHandler(this.ButtonLocalFunctions_Click);
            // 
            // ButtonParams
            // 
            this.ButtonParams.Location = new System.Drawing.Point(975, 633);
            this.ButtonParams.Name = "ButtonParams";
            this.ButtonParams.Size = new System.Drawing.Size(124, 23);
            this.ButtonParams.TabIndex = 38;
            this.ButtonParams.Text = "Params";
            this.ButtonParams.UseVisualStyleBackColor = true;
            this.ButtonParams.Click += new System.EventHandler(this.ButtonParams_Click);
            // 
            // ButtonCheckParameter
            // 
            this.ButtonCheckParameter.Location = new System.Drawing.Point(687, 133);
            this.ButtonCheckParameter.Name = "ButtonCheckParameter";
            this.ButtonCheckParameter.Size = new System.Drawing.Size(124, 23);
            this.ButtonCheckParameter.TabIndex = 37;
            this.ButtonCheckParameter.Text = "參數檢查";
            this.ButtonCheckParameter.UseVisualStyleBackColor = true;
            this.ButtonCheckParameter.Click += new System.EventHandler(this.ButtonCheckParameter_Click);
            // 
            // ButtonValueTuple
            // 
            this.ButtonValueTuple.Location = new System.Drawing.Point(687, 89);
            this.ButtonValueTuple.Name = "ButtonValueTuple";
            this.ButtonValueTuple.Size = new System.Drawing.Size(124, 23);
            this.ButtonValueTuple.TabIndex = 36;
            this.ButtonValueTuple.Text = "ValueTuple";
            this.ButtonValueTuple.UseVisualStyleBackColor = true;
            this.ButtonValueTuple.Click += new System.EventHandler(this.ButtonValueTuple_Click);
            // 
            // ButtonRef
            // 
            this.ButtonRef.Location = new System.Drawing.Point(419, 211);
            this.ButtonRef.Name = "ButtonRef";
            this.ButtonRef.Size = new System.Drawing.Size(124, 23);
            this.ButtonRef.TabIndex = 35;
            this.ButtonRef.Text = "Ref";
            this.ButtonRef.UseVisualStyleBackColor = true;
            this.ButtonRef.Click += new System.EventHandler(this.ButtonRef_Click);
            // 
            // ButtonOverloading
            // 
            this.ButtonOverloading.Location = new System.Drawing.Point(549, 281);
            this.ButtonOverloading.Name = "ButtonOverloading";
            this.ButtonOverloading.Size = new System.Drawing.Size(124, 23);
            this.ButtonOverloading.TabIndex = 34;
            this.ButtonOverloading.Text = "Overloading";
            this.ButtonOverloading.UseVisualStyleBackColor = true;
            this.ButtonOverloading.Click += new System.EventHandler(this.ButtonOverloading_Click);
            // 
            // ButtonCallByReferenceValue
            // 
            this.ButtonCallByReferenceValue.Location = new System.Drawing.Point(419, 170);
            this.ButtonCallByReferenceValue.Name = "ButtonCallByReferenceValue";
            this.ButtonCallByReferenceValue.Size = new System.Drawing.Size(124, 23);
            this.ButtonCallByReferenceValue.TabIndex = 33;
            this.ButtonCallByReferenceValue.Text = "Call by reference,value";
            this.ButtonCallByReferenceValue.UseVisualStyleBackColor = true;
            this.ButtonCallByReferenceValue.Click += new System.EventHandler(this.ButtonCallByReferenceValue_Click);
            // 
            // ButtonImplicitConversions
            // 
            this.ButtonImplicitConversions.Location = new System.Drawing.Point(549, 404);
            this.ButtonImplicitConversions.Name = "ButtonImplicitConversions";
            this.ButtonImplicitConversions.Size = new System.Drawing.Size(124, 23);
            this.ButtonImplicitConversions.TabIndex = 44;
            this.ButtonImplicitConversions.Text = "隱含轉型";
            this.ButtonImplicitConversions.UseVisualStyleBackColor = true;
            this.ButtonImplicitConversions.Click += new System.EventHandler(this.ButtonImplicitConversions_Click);
            // 
            // ButtonConversionRightWay
            // 
            this.ButtonConversionRightWay.Location = new System.Drawing.Point(549, 490);
            this.ButtonConversionRightWay.Name = "ButtonConversionRightWay";
            this.ButtonConversionRightWay.Size = new System.Drawing.Size(124, 23);
            this.ButtonConversionRightWay.TabIndex = 43;
            this.ButtonConversionRightWay.Text = "正確轉型方法";
            this.ButtonConversionRightWay.UseVisualStyleBackColor = true;
            this.ButtonConversionRightWay.Click += new System.EventHandler(this.ButtonConversionRightWay_Click);
            // 
            // ButtonBoxingUnboxing
            // 
            this.ButtonBoxingUnboxing.Location = new System.Drawing.Point(419, 250);
            this.ButtonBoxingUnboxing.Name = "ButtonBoxingUnboxing";
            this.ButtonBoxingUnboxing.Size = new System.Drawing.Size(124, 23);
            this.ButtonBoxingUnboxing.TabIndex = 42;
            this.ButtonBoxingUnboxing.Text = "BoxingUnboxing";
            this.ButtonBoxingUnboxing.UseVisualStyleBackColor = true;
            this.ButtonBoxingUnboxing.Click += new System.EventHandler(this.ButtonBoxingUnboxing_Click);
            // 
            // ButtonExplicitConversions
            // 
            this.ButtonExplicitConversions.Location = new System.Drawing.Point(549, 448);
            this.ButtonExplicitConversions.Name = "ButtonExplicitConversions";
            this.ButtonExplicitConversions.Size = new System.Drawing.Size(124, 23);
            this.ButtonExplicitConversions.TabIndex = 41;
            this.ButtonExplicitConversions.Text = "明確轉型";
            this.ButtonExplicitConversions.UseVisualStyleBackColor = true;
            this.ButtonExplicitConversions.Click += new System.EventHandler(this.ButtonExplicitConversions_Click);
            // 
            // ButtonMethodLength
            // 
            this.ButtonMethodLength.Location = new System.Drawing.Point(687, 211);
            this.ButtonMethodLength.Name = "ButtonMethodLength";
            this.ButtonMethodLength.Size = new System.Drawing.Size(124, 23);
            this.ButtonMethodLength.TabIndex = 45;
            this.ButtonMethodLength.Text = "方法長度";
            this.ButtonMethodLength.UseVisualStyleBackColor = true;
            this.ButtonMethodLength.Click += new System.EventHandler(this.ButtonMethodLength_Click);
            // 
            // ButtonStringImmutable
            // 
            this.ButtonStringImmutable.Location = new System.Drawing.Point(836, 133);
            this.ButtonStringImmutable.Name = "ButtonStringImmutable";
            this.ButtonStringImmutable.Size = new System.Drawing.Size(124, 23);
            this.ButtonStringImmutable.TabIndex = 51;
            this.ButtonStringImmutable.Text = "字串不變性";
            this.ButtonStringImmutable.UseVisualStyleBackColor = true;
            this.ButtonStringImmutable.Click += new System.EventHandler(this.ButtonStringImmutable_Click);
            // 
            // ButtonStringSearch
            // 
            this.ButtonStringSearch.Location = new System.Drawing.Point(836, 260);
            this.ButtonStringSearch.Name = "ButtonStringSearch";
            this.ButtonStringSearch.Size = new System.Drawing.Size(124, 23);
            this.ButtonStringSearch.TabIndex = 50;
            this.ButtonStringSearch.Text = "字串尋找演算法";
            this.ButtonStringSearch.UseVisualStyleBackColor = true;
            this.ButtonStringSearch.Click += new System.EventHandler(this.ButtonStringSearch_Click);
            // 
            // ButtonStringEmpty
            // 
            this.ButtonStringEmpty.Location = new System.Drawing.Point(270, 423);
            this.ButtonStringEmpty.Name = "ButtonStringEmpty";
            this.ButtonStringEmpty.Size = new System.Drawing.Size(124, 23);
            this.ButtonStringEmpty.TabIndex = 49;
            this.ButtonStringEmpty.Text = "空字串表示";
            this.ButtonStringEmpty.UseVisualStyleBackColor = true;
            this.ButtonStringEmpty.Click += new System.EventHandler(this.ButtonStringEmpty_Click);
            // 
            // ButtonStringCompare
            // 
            this.ButtonStringCompare.Location = new System.Drawing.Point(836, 216);
            this.ButtonStringCompare.Name = "ButtonStringCompare";
            this.ButtonStringCompare.Size = new System.Drawing.Size(124, 23);
            this.ButtonStringCompare.TabIndex = 48;
            this.ButtonStringCompare.Text = "字串比較";
            this.ButtonStringCompare.UseVisualStyleBackColor = true;
            this.ButtonStringCompare.Click += new System.EventHandler(this.ButtonStringCompare_Click);
            // 
            // ButtonAt
            // 
            this.ButtonAt.Location = new System.Drawing.Point(836, 83);
            this.ButtonAt.Name = "ButtonAt";
            this.ButtonAt.Size = new System.Drawing.Size(124, 23);
            this.ButtonAt.TabIndex = 47;
            this.ButtonAt.Text = "@";
            this.ButtonAt.UseVisualStyleBackColor = true;
            this.ButtonAt.Click += new System.EventHandler(this.ButtonAt_Click);
            // 
            // ButtonStringConcat
            // 
            this.ButtonStringConcat.Location = new System.Drawing.Point(836, 172);
            this.ButtonStringConcat.Name = "ButtonStringConcat";
            this.ButtonStringConcat.Size = new System.Drawing.Size(124, 23);
            this.ButtonStringConcat.TabIndex = 46;
            this.ButtonStringConcat.Text = "字串串接";
            this.ButtonStringConcat.UseVisualStyleBackColor = true;
            this.ButtonStringConcat.Click += new System.EventHandler(this.ButtonStringConcat_Click);
            // 
            // ButtonMemoryAllocate
            // 
            this.ButtonMemoryAllocate.Location = new System.Drawing.Point(419, 89);
            this.ButtonMemoryAllocate.Name = "ButtonMemoryAllocate";
            this.ButtonMemoryAllocate.Size = new System.Drawing.Size(124, 23);
            this.ButtonMemoryAllocate.TabIndex = 54;
            this.ButtonMemoryAllocate.Text = "記憶體配置";
            this.ButtonMemoryAllocate.UseVisualStyleBackColor = true;
            this.ButtonMemoryAllocate.Click += new System.EventHandler(this.ButtonMemoryAllocate_Click);
            // 
            // ButtonStatic
            // 
            this.ButtonStatic.Location = new System.Drawing.Point(419, 129);
            this.ButtonStatic.Name = "ButtonStatic";
            this.ButtonStatic.Size = new System.Drawing.Size(124, 23);
            this.ButtonStatic.TabIndex = 53;
            this.ButtonStatic.Text = "靜態";
            this.ButtonStatic.UseVisualStyleBackColor = true;
            this.ButtonStatic.Click += new System.EventHandler(this.ButtonStatic_Click);
            // 
            // ButtonThreadSafe
            // 
            this.ButtonThreadSafe.Location = new System.Drawing.Point(975, 85);
            this.ButtonThreadSafe.Name = "ButtonThreadSafe";
            this.ButtonThreadSafe.Size = new System.Drawing.Size(124, 23);
            this.ButtonThreadSafe.TabIndex = 52;
            this.ButtonThreadSafe.Text = "Thread Safe";
            this.ButtonThreadSafe.UseVisualStyleBackColor = true;
            this.ButtonThreadSafe.Click += new System.EventHandler(this.ButtonThreadSafe_Click);
            // 
            // ButtonGoto
            // 
            this.ButtonGoto.Location = new System.Drawing.Point(975, 531);
            this.ButtonGoto.Name = "ButtonGoto";
            this.ButtonGoto.Size = new System.Drawing.Size(124, 23);
            this.ButtonGoto.TabIndex = 62;
            this.ButtonGoto.Text = "goto";
            this.ButtonGoto.UseVisualStyleBackColor = true;
            this.ButtonGoto.Click += new System.EventHandler(this.ButtonGoto_Click);
            // 
            // ButtonDoEvents
            // 
            this.ButtonDoEvents.Location = new System.Drawing.Point(975, 584);
            this.ButtonDoEvents.Name = "ButtonDoEvents";
            this.ButtonDoEvents.Size = new System.Drawing.Size(124, 23);
            this.ButtonDoEvents.TabIndex = 61;
            this.ButtonDoEvents.Text = "DoEvents";
            this.ButtonDoEvents.UseVisualStyleBackColor = true;
            this.ButtonDoEvents.Click += new System.EventHandler(this.ButtonDoEvents_Click);
            // 
            // ButtonGC
            // 
            this.ButtonGC.Location = new System.Drawing.Point(975, 260);
            this.ButtonGC.Name = "ButtonGC";
            this.ButtonGC.Size = new System.Drawing.Size(124, 23);
            this.ButtonGC.TabIndex = 60;
            this.ButtonGC.Text = "GC";
            this.ButtonGC.UseVisualStyleBackColor = true;
            this.ButtonGC.Click += new System.EventHandler(this.ButtonGC_Click);
            // 
            // ButtonRaceCondition
            // 
            this.ButtonRaceCondition.Location = new System.Drawing.Point(975, 133);
            this.ButtonRaceCondition.Name = "ButtonRaceCondition";
            this.ButtonRaceCondition.Size = new System.Drawing.Size(124, 23);
            this.ButtonRaceCondition.TabIndex = 59;
            this.ButtonRaceCondition.Text = "Race condition";
            this.ButtonRaceCondition.UseVisualStyleBackColor = true;
            this.ButtonRaceCondition.Click += new System.EventHandler(this.ButtonRaceCondition_Click);
            // 
            // ButtonFloatingPointNumber
            // 
            this.ButtonFloatingPointNumber.Location = new System.Drawing.Point(549, 133);
            this.ButtonFloatingPointNumber.Name = "ButtonFloatingPointNumber";
            this.ButtonFloatingPointNumber.Size = new System.Drawing.Size(124, 23);
            this.ButtonFloatingPointNumber.TabIndex = 58;
            this.ButtonFloatingPointNumber.Text = "浮點數誤差";
            this.ButtonFloatingPointNumber.UseVisualStyleBackColor = true;
            this.ButtonFloatingPointNumber.Click += new System.EventHandler(this.ButtonFloatingPointNumber_Click);
            // 
            // ButtonDelegateAndEvent
            // 
            this.ButtonDelegateAndEvent.Location = new System.Drawing.Point(975, 216);
            this.ButtonDelegateAndEvent.Name = "ButtonDelegateAndEvent";
            this.ButtonDelegateAndEvent.Size = new System.Drawing.Size(124, 23);
            this.ButtonDelegateAndEvent.TabIndex = 57;
            this.ButtonDelegateAndEvent.Text = "Delegate and Event";
            this.ButtonDelegateAndEvent.UseVisualStyleBackColor = true;
            this.ButtonDelegateAndEvent.Click += new System.EventHandler(this.ButtonDelegateAndEvent_Click);
            // 
            // ButtonDispose
            // 
            this.ButtonDispose.Location = new System.Drawing.Point(975, 41);
            this.ButtonDispose.Name = "ButtonDispose";
            this.ButtonDispose.Size = new System.Drawing.Size(124, 23);
            this.ButtonDispose.TabIndex = 56;
            this.ButtonDispose.Text = "釋放資源";
            this.ButtonDispose.UseVisualStyleBackColor = true;
            this.ButtonDispose.Click += new System.EventHandler(this.ButtonDispose_Click);
            // 
            // ButtonException
            // 
            this.ButtonException.Location = new System.Drawing.Point(975, 177);
            this.ButtonException.Name = "ButtonException";
            this.ButtonException.Size = new System.Drawing.Size(124, 23);
            this.ButtonException.TabIndex = 55;
            this.ButtonException.Text = "例外";
            this.ButtonException.UseVisualStyleBackColor = true;
            this.ButtonException.Click += new System.EventHandler(this.ButtonException_Click);
            // 
            // ButtonExtendMethod
            // 
            this.ButtonExtendMethod.Location = new System.Drawing.Point(836, 294);
            this.ButtonExtendMethod.Name = "ButtonExtendMethod";
            this.ButtonExtendMethod.Size = new System.Drawing.Size(124, 23);
            this.ButtonExtendMethod.TabIndex = 63;
            this.ButtonExtendMethod.Text = "擴充方法";
            this.ButtonExtendMethod.UseVisualStyleBackColor = true;
            this.ButtonExtendMethod.Click += new System.EventHandler(this.ButtonExtendMethod_Click);
            // 
            // ButtonGenerics
            // 
            this.ButtonGenerics.Location = new System.Drawing.Point(549, 322);
            this.ButtonGenerics.Name = "ButtonGenerics";
            this.ButtonGenerics.Size = new System.Drawing.Size(124, 23);
            this.ButtonGenerics.TabIndex = 64;
            this.ButtonGenerics.Text = "泛型";
            this.ButtonGenerics.UseVisualStyleBackColor = true;
            this.ButtonGenerics.Click += new System.EventHandler(this.ButtonGenerics_Click);
            // 
            // ButtonOptionParameter
            // 
            this.ButtonOptionParameter.Location = new System.Drawing.Point(687, 41);
            this.ButtonOptionParameter.Name = "ButtonOptionParameter";
            this.ButtonOptionParameter.Size = new System.Drawing.Size(124, 23);
            this.ButtonOptionParameter.TabIndex = 65;
            this.ButtonOptionParameter.Text = "選擇性參數";
            this.ButtonOptionParameter.UseVisualStyleBackColor = true;
            this.ButtonOptionParameter.Click += new System.EventHandler(this.ButtonOptionParameter_Click);
            // 
            // ButtonCSharp
            // 
            this.ButtonCSharp.Location = new System.Drawing.Point(133, 85);
            this.ButtonCSharp.Name = "ButtonCSharp";
            this.ButtonCSharp.Size = new System.Drawing.Size(124, 23);
            this.ButtonCSharp.TabIndex = 66;
            this.ButtonCSharp.Text = "C#";
            this.ButtonCSharp.UseVisualStyleBackColor = true;
            this.ButtonCSharp.Click += new System.EventHandler(this.ButtonCSharp_Click);
            // 
            // buttonSolution
            // 
            this.buttonSolution.Location = new System.Drawing.Point(133, 250);
            this.buttonSolution.Name = "buttonSolution";
            this.buttonSolution.Size = new System.Drawing.Size(124, 23);
            this.buttonSolution.TabIndex = 67;
            this.buttonSolution.Text = "專案結構";
            this.buttonSolution.UseVisualStyleBackColor = true;
            this.buttonSolution.Click += new System.EventHandler(this.buttonSolution_Click);
            // 
            // buttonObjectOriented
            // 
            this.buttonObjectOriented.Location = new System.Drawing.Point(133, 129);
            this.buttonObjectOriented.Name = "buttonObjectOriented";
            this.buttonObjectOriented.Size = new System.Drawing.Size(124, 23);
            this.buttonObjectOriented.TabIndex = 68;
            this.buttonObjectOriented.Text = "物件導向";
            this.buttonObjectOriented.UseVisualStyleBackColor = true;
            this.buttonObjectOriented.Click += new System.EventHandler(this.buttonObjectOriented_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1143, 746);
            this.Controls.Add(this.buttonObjectOriented);
            this.Controls.Add(this.buttonSolution);
            this.Controls.Add(this.ButtonCSharp);
            this.Controls.Add(this.ButtonOptionParameter);
            this.Controls.Add(this.ButtonGenerics);
            this.Controls.Add(this.ButtonExtendMethod);
            this.Controls.Add(this.ButtonGoto);
            this.Controls.Add(this.ButtonDoEvents);
            this.Controls.Add(this.ButtonGC);
            this.Controls.Add(this.ButtonRaceCondition);
            this.Controls.Add(this.ButtonFloatingPointNumber);
            this.Controls.Add(this.ButtonDelegateAndEvent);
            this.Controls.Add(this.ButtonDispose);
            this.Controls.Add(this.ButtonException);
            this.Controls.Add(this.ButtonMemoryAllocate);
            this.Controls.Add(this.ButtonStatic);
            this.Controls.Add(this.ButtonThreadSafe);
            this.Controls.Add(this.ButtonStringImmutable);
            this.Controls.Add(this.ButtonStringSearch);
            this.Controls.Add(this.ButtonStringEmpty);
            this.Controls.Add(this.ButtonStringCompare);
            this.Controls.Add(this.ButtonAt);
            this.Controls.Add(this.ButtonStringConcat);
            this.Controls.Add(this.ButtonMethodLength);
            this.Controls.Add(this.ButtonImplicitConversions);
            this.Controls.Add(this.ButtonConversionRightWay);
            this.Controls.Add(this.ButtonBoxingUnboxing);
            this.Controls.Add(this.ButtonExplicitConversions);
            this.Controls.Add(this.ButtonRepeatCall);
            this.Controls.Add(this.ButtonLocalFunctions);
            this.Controls.Add(this.ButtonParams);
            this.Controls.Add(this.ButtonCheckParameter);
            this.Controls.Add(this.ButtonValueTuple);
            this.Controls.Add(this.ButtonRef);
            this.Controls.Add(this.ButtonOverloading);
            this.Controls.Add(this.ButtonCallByReferenceValue);
            this.Controls.Add(this.ButtonDynamic);
            this.Controls.Add(this.ButtonVar);
            this.Controls.Add(this.ButtonAnonymousType);
            this.Controls.Add(this.ButtonNullableType);
            this.Controls.Add(this.ButtonDoubleSpecialValue);
            this.Controls.Add(this.MessageRichBox);
            this.Controls.Add(this.ButtonStrongTypeLanguage);
            this.Controls.Add(this.ButtonValueReferenceType);
            this.Controls.Add(this.ButtonMagicNumber);
            this.Controls.Add(this.ButtonDuplicateCode);
            this.Controls.Add(this.ButtonClassLocationRule);
            this.Controls.Add(this.ButtonGlobalItem);
            this.Controls.Add(this.ButtonCurlyBrackets);
            this.Controls.Add(this.ButtonDanglingElse);
            this.Controls.Add(this.ButtonDeclareVariable);
            this.Controls.Add(this.ButtonNewlineSeparate);
            this.Controls.Add(this.ButtonCSharpType);
            this.Controls.Add(this.ButtonDeliveryPrinciple);
            this.Controls.Add(this.ButtonName);
            this.Controls.Add(this.ButtonCommentOut);
            this.Controls.Add(this.ButtonTaskList);
            this.Controls.Add(this.ButtonFieldProperty);
            this.Controls.Add(this.ButtonAccessModifiers);
            this.Controls.Add(this.ButtonClassMembers);
            this.Controls.Add(this.ButtonPurpose);
            this.Name = "Form1";
            this.Text = "Coding Style Demo";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButtonPurpose;
        private System.Windows.Forms.Button ButtonClassMembers;
        private System.Windows.Forms.Button ButtonAccessModifiers;
        private System.Windows.Forms.Button ButtonFieldProperty;
        private System.Windows.Forms.Button ButtonTaskList;
        private System.Windows.Forms.Button ButtonCommentOut;
        private System.Windows.Forms.Button ButtonName;
        private System.Windows.Forms.Button ButtonDeliveryPrinciple;
        private System.Windows.Forms.Button ButtonCSharpType;
        private System.Windows.Forms.Button ButtonNewlineSeparate;
        private System.Windows.Forms.Button ButtonDeclareVariable;
        private System.Windows.Forms.Button ButtonDanglingElse;
        private System.Windows.Forms.Button ButtonCurlyBrackets;
        private System.Windows.Forms.Button ButtonGlobalItem;
        private System.Windows.Forms.Button ButtonClassLocationRule;
        private System.Windows.Forms.Button ButtonDuplicateCode;
        private System.Windows.Forms.Button ButtonMagicNumber;
        private System.Windows.Forms.Button ButtonStrongTypeLanguage;
        private System.Windows.Forms.Button ButtonValueReferenceType;
        private System.Windows.Forms.RichTextBox MessageRichBox;
        private System.Windows.Forms.Button ButtonNullableType;
        private System.Windows.Forms.Button ButtonDoubleSpecialValue;
        private System.Windows.Forms.Button ButtonDynamic;
        private System.Windows.Forms.Button ButtonVar;
        private System.Windows.Forms.Button ButtonAnonymousType;
        private System.Windows.Forms.Button ButtonRepeatCall;
        private System.Windows.Forms.Button ButtonLocalFunctions;
        private System.Windows.Forms.Button ButtonParams;
        private System.Windows.Forms.Button ButtonCheckParameter;
        private System.Windows.Forms.Button ButtonValueTuple;
        private System.Windows.Forms.Button ButtonRef;
        private System.Windows.Forms.Button ButtonOverloading;
        private System.Windows.Forms.Button ButtonCallByReferenceValue;
        private System.Windows.Forms.Button ButtonImplicitConversions;
        private System.Windows.Forms.Button ButtonConversionRightWay;
        private System.Windows.Forms.Button ButtonBoxingUnboxing;
        private System.Windows.Forms.Button ButtonExplicitConversions;
        private System.Windows.Forms.Button ButtonMethodLength;
        private System.Windows.Forms.Button ButtonStringImmutable;
        private System.Windows.Forms.Button ButtonStringSearch;
        private System.Windows.Forms.Button ButtonStringEmpty;
        private System.Windows.Forms.Button ButtonStringCompare;
        private System.Windows.Forms.Button ButtonAt;
        private System.Windows.Forms.Button ButtonStringConcat;
        private System.Windows.Forms.Button ButtonMemoryAllocate;
        private System.Windows.Forms.Button ButtonStatic;
        private System.Windows.Forms.Button ButtonThreadSafe;
        private System.Windows.Forms.Button ButtonGoto;
        private System.Windows.Forms.Button ButtonDoEvents;
        private System.Windows.Forms.Button ButtonGC;
        private System.Windows.Forms.Button ButtonRaceCondition;
        private System.Windows.Forms.Button ButtonFloatingPointNumber;
        private System.Windows.Forms.Button ButtonDelegateAndEvent;
        private System.Windows.Forms.Button ButtonDispose;
        private System.Windows.Forms.Button ButtonException;
        private System.Windows.Forms.Button ButtonExtendMethod;
        private System.Windows.Forms.Button ButtonGenerics;
        private System.Windows.Forms.Button ButtonOptionParameter;
        private System.Windows.Forms.Button ButtonCSharp;
        private System.Windows.Forms.Button buttonSolution;
        private System.Windows.Forms.Button buttonObjectOriented;
    }
}


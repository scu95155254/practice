﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CodingStyleDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ButtonPurpose_Click(object sender, EventArgs e)
        {
            //彼此好維護：確保一致性、易讀性
            //效能更好：了解執行的成本
            //更安全：避免掉不安全的作法
        }

        

        private void ButtonClassMembers_Click(object sender, EventArgs e)
        {
            ClassMembers classMembers = new ClassMembers();
        }

        private void ButtonAccessModifiers_Click(object sender, EventArgs e)
        {
            AccessModifiers accessModifiers = new AccessModifiers();
        }

        private void ButtonFieldProperty_Click(object sender, EventArgs e)
        {
            ClassMembers.Shop shop = new ClassMembers.Shop();
            shop.CakeDiscountRate = 0.1;
            //shop.CookieDiscountRate = 0.2;
        }

        private void ButtonMagicNumber_Click(object sender, EventArgs e)
        {
            //什麼是Magic Number？
            MagicNumber magicNumber = new MagicNumber();
        }

        private void ButtonDuplicateCode_Click(object sender, EventArgs e)
        {
            //Don't copy
            //3個壞處，1個警訊！
            
        }

        private void ButtonClassLocationRule_Click(object sender, EventArgs e)
        {
            //one cs file one class，除了巢狀類別、POCO(允許)、Partial(禁用)

            //POCO
            Restaurant restaurant = new Restaurant();

            //Partial
            //本專案
        }

        private void ButtonGlobalItem_Click(object sender, EventArgs e)
        {
            //Global item → const method
            Global.SendMail(Global.ServerEmail, "寄信測試");
        }

        private void ButtonCurlyBrackets_Click(object sender, EventArgs e)
        {
            CurlyBrackets.IfMustAddCurlyBrackets();
            CurlyBrackets.SingleLine();
        }

        private void ButtonDanglingElse_Click(object sender, EventArgs e)
        {


            bool condition1 = false;
            bool condition2 = true;

            if (condition1)
                MessageBox.Show("00");
            MessageBox.Show("01");
            int num = 2;

            if (num == 3)
                if (num == 5)
                    MessageBox.Show("1");
            else
                MessageBox.Show("2");

            MessageBox.Show("Start Next");

            if (condition1)
            {
                if (condition2)
                {
                    MessageBox.Show("1");
                }
            }
            else
            {
                MessageBox.Show("2");
            }


            //else無法確定要與第一個或第二個 if 結合，這種現象就是所謂的dangling else問題
        }

        private void ButtonDeclareVariable_Click(object sender, EventArgs e)
        {
            //一個變數宣告就佔一行，一致性

            //使用
            int a = 1;
            int b = 2;
            int c = 3;

            //不使用
            int x = 1, y = 2, z = 3;
        }

        private void ButtonNewlineSeparate_Click(object sender, EventArgs e)
        {
            //準備資料、初始化
            int count = 0;
            List<string> datas = new List<string>();

            //執行邏輯
            foreach (string item in datas)
            {

            }

            //紀錄回報
            Global.SendMail(Global.ServerEmail, "執行完成");
        }

        private void ButtonCSharpType_Click(object sender, EventArgs e)
        {
            //使用 C#內置型別，而非 .NET Class library (friend for c like)
            //一致性、微軟也建議的寫法
            //short NOT System.Int16
            //int NOT System.Int32
            //long NOT System.Int64
            //string NOT System.String
        }

        private void ButtonDeliveryPrinciple_Click(object sender, EventArgs e)
        {
            //不該交出 Compile 後有一堆警告的程式碼，更不該交出有 Error 的程式碼
        }

        private void ButtonName_Click(object sender, EventArgs e)
        {
            //程式碼除了註解與字串外，一律使用英文命名
            //除非中文命名的理解遠超過英文
            //AssociationForRelationsAcrossTheTaiwanStraits

            #region 格式
            //常數成員一律 全大寫配底線分隔單字 => VERSION_NUMBER

            //區域變數、方法參數，小寫開頭駝式命名 => computerGame

            //其餘，全部大寫開頭駝式命名 => BasketballGame 
            #endregion

            //方法：動詞開頭，類別與其他類別成員：名詞

            //bool，必須含有判斷意義，用 Is Can Has 等詞開頭

            //不要多餘
            int iCount;
            long lSize;

        //    public enum ColorEnum { .... }      //enum
        //public class MachineClass { .... }  //class

        //public class Item
        //{
        //    //以下所有的屬性，前置 Item 字樣都是多餘的 
        //    public int ItemId { get; set; }
        //    public string ItemName { get; set; }
        //    public byte[] ItemData { get; set; }
        //}

        //使用有意義的命名，該命名盡可能附合該變數所具有的功能
        //    a, b, c, temp1, temp2, nameA, nameB 這一類的命名方式都是不好的

        //七。i, j, k, (x, y, z) (r, g, b, a) 這種變數，只可以用在迴圈步進變數或座標指定這種特定用法
        //   備註:也不可使用於Linq的Lambda，請明確定義lambda裡面變數名稱，不要使用x、y、z

        //五。縮寫必須是大家都一致認知的，不可任意自訂，如果不是過長的命名，建議少用縮寫
        //    自創五個字元以上的縮寫禁用 (目前我們並沒有提供常用縮寫表)
        //    常見 No Num Id ASCII A2B() => AToB()

        //六。禁止定義任何類別的名稱與 .Net 定義相同，如 List, Dictionary, DataTable... 
        //    看code的人太容易把它當 .Net 內建來使用

        //八。只有函式內的區域變數可以使用 temp 這一類的通用暫時變數命名，禁止在類別級成員使用

        //九。Interface 的命名一律以 I 開頭，泛型的型別參數一律使用 <T>  (base on .Net)

        //十。命名空間的名字，須要和專案檔與輸出組件名稱一致
    }

        /// <summary>
        /// 我是XML註解，一定要寫!!!
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ButtonCommentOut_Click(object sender, EventArgs e)
        {
            //單行註解

            /*
             * 多行註解
             * 多行註解
             * 多行註解
             * 多行註解
             */

            //更推薦使用
            //多行註解
            //多行註解
            //多行註解
            //多行註解

            //不要這樣...
            //*******************************
            //................
            //*******************************

            //好的程式碼和註解要作到 Self documenting
        }

        private void ButtonTaskList_Click(object sender, EventArgs e)
        {
            //善用 Visual Studio 提供的 TaskList 功能

            //TODO：Place Database Code Here
            //UNDONE Removed P\Invoke Call due to errors
            //HACK: Temporary fix until able to refactor
        }

        private void ButtonStrongTypeLanguage_Click(object sender, EventArgs e)
        {
            //程式語言？概念？

            //C# 是強型別語言
            int i = 1;
            //i = "A";

            //StrongType觀念，是指「盡量使用具有型別的方式開發」

            //好處：在「編譯時期(Compile-Time)」就能夠發現錯誤

            //避免使用弱型別寫法
        }

        private void ButtonValueReferenceType_Click(object sender, EventArgs e)
        {
            //Value Type
            //bool、byte、sbyte、char、decimal、double、float、int、uint、long、ulong、short、ushort
            //enum
            //struct
            //https://www.tutorialsteacher.com/csharp/csharp-value-type-and-reference-type
        }

        private void ButtonNullableType_Click(object sender, EventArgs e)
        {
            //意義 0 跟 null 的差異是什麼

            int? a ;
            int? b = 1;

            //DB可null，所以實務上也需要可null的value type

            //只能是value type使用，而且他是value type
        }
        private void ButtonFloatingPointNumber_Click(object sender, EventArgs e)
        {
            //浮點數怎麼算

            //2.37-> 10.10

            //    .01 *10 ^1
        }

        private void ButtonDoubleSpecialValue_Click(object sender, EventArgs e)
        {
            //http://lectures.molgen.mpg.de/AlgoStoc/ieee.html
        }

        private void ButtonAnonymousType_Click(object sender, EventArgs e)
        {
            //目的

            //生命週期

            #region 範例
            DateTime now = DateTime.Now;
            var dog = new { Color = "yellow", Weight = 5, RecordTime = now };

            #endregion
        }

        private void ButtonVar_Click(object sender, EventArgs e)
        {
            var count = 1;
            var bigCount = 1L;
            
        }

        private void ButtonBoxingUnboxing_Click(object sender, EventArgs e)
        {
            //https://docs.microsoft.com/zh-tw/previous-versions/yz2be5wk(v=vs.120)
            int loop = 1000000000;
            int a = 1;
            int b;
            object bb;

            int st1 = Environment.TickCount;
            for (int i = 0; i < loop; i++)
            {
                //value assign
                b = a;
            }
            st1 = Environment.TickCount - st1;
            MessageBox.Show(string.Format("value to value {0} ms", st1));

            int st2 = Environment.TickCount;
            for (int i = 0; i < loop; i++)
            {
                //boxing 
                bb = a;
            }

            st2 = Environment.TickCount - st2;
            MessageBox.Show(string.Format("value to object {0} ms", st2));

            MessageBox.Show(string.Format("效率差：{0:f2} %", (st2 - st1) / (st2 * 0.01)));
        }

        private void ButtonImplicitConversions_Click(object sender, EventArgs e)
        {
            //不必使用特殊語法，因為這類轉換屬於型別安全轉換，而且不會遺失資料，稱之隱含轉換。

            //value type
            //從小到大
            int smallNumber = 1;
            long bigNumber = smallNumber;

            //reference type( need inheritance)
            //從衍生類別 (Derived Class) 到基底類別 (Base Class) 的轉換。
            DerivedSample derivedSample = new DerivedSample();
            BaseSample baseSample = derivedSample;
        }

        private void ButtonExplicitConversions_Click(object sender, EventArgs e)
        {
            //不安全轉換，可能遺失資料，所以要求撰寫cast程式碼，稱之明確轉換。

            //value type
            //從大到小
            long bigNumber = 1L;
            int smallNumber = (int)bigNumber;

            //reference type( need inheritance)
            //從 基底類別(Base Class) 到 衍生類別(Derived Class) 的轉換。
            BaseSample baseSample = new BaseSample();
            DerivedSample derivedSample = (DerivedSample)baseSample;
            //有可能檢查不到危險的「參考型別」轉換Orange => Fruit => Apple 編譯時期會過 但runtime時可能會造成非預期結果
        }

        private void ButtonConversionRightWay_Click(object sender, EventArgs e)
        {
            //有 as is，is 更建議使用，更簡短, 語意更合乎英文文法


            BaseSample baseSample = new BaseSample();

            //is (Good)
            if (baseSample is DerivedSample derivedSample)
            {
                int id = derivedSample.Id;
            }

            if (baseSample is DerivedSample)
            {
                DerivedSample derivedSample2 = (DerivedSample)baseSample;
                int id = derivedSample2.Id;
            }

            //as (Bad)
            BaseSample baseSample2 = new BaseSample();
            DerivedSample derivedSample3 = baseSample2 as DerivedSample;
            if (derivedSample3 != null)
            {
                int id = derivedSample3.Id;
            }
        }

        private void ButtonGenerics_Click(object sender, EventArgs e)
        {
            //好東西

            //為什麼要存在

            
        }

        private void ButtonOverloading_Click(object sender, EventArgs e)
        {
            //請用 overloading 來增加方法能力或版本相容
        }

        private void ButtonOptionParameter_Click(object sender, EventArgs e)
        {
            //選擇性參數與具名引數
            OptionalParamterDemo.SetValueOptionalParamter("值");
            OptionalParamterDemo.SetValueOptionalParamter("值", timeout2: 30);
            var employee = new { Id = 1, Name = "Marcus", Age = 22 };

            OptionalParamterDemo.SetValueRealParamterName(timeout: 30, originalValue: "原始值", value: "值");
        }

        private void ButtonCallByReferenceValue_Click(object sender, EventArgs e)
        {
            int value = 0;

            Food food = new Food()
            {
                Name = "薯條"
            };


            CallByReferenceValue.SetValue(value);
            Console.WriteLine(food.Name);
            CallByReferenceValue.SetValue(food);
            Console.WriteLine(food.Name);
            CallByReferenceValue.SetValueNew(food);
            Console.WriteLine(food.Name);

        }


        private void ButtonRef_Click(object sender, EventArgs e)
        {
            int value = 0;

            Food food = new Food()
            {
                Name = "薯條"
            };

            CallByReferenceValue.SetValue(ref value);
            Console.WriteLine(food.Name);
            CallByReferenceValue.SetValue(ref food);
            Console.WriteLine(food.Name);
            CallByReferenceValue.SetValueNew(ref food);
        }

        private void ButtonStatic_Click(object sender, EventArgs e)
        {
            //靜態就是static

            //靜態變數 in Data Segment

            //靜態類別無法建 instance

            //靜態函式不屬於任何一個物件

            //靜態VS.非靜態

            //靜態類別、靜態類別成員、靜態函式

            //靜態類別用途??小心Thread Safe
        }

        private void ButtonDynamic_Click(object sender, EventArgs e)
        {

        }

        private void ButtonStringImmutable_Click(object sender, EventArgs e)
        {
            string name = "Jake";
            CallByReferenceValue.SetValue(name);

            CallByReferenceValue.SetValue(ref name);
        }

        private void ButtonValueTuple_Click(object sender, EventArgs e)
        {
            //Tuple<int, string> tuple = ValueTupleDemo.GetDate();
            //(int sum, string name) valueTuple = ValueTupleDemo.GetDateValueTuple();
            //valueTuple.sum = 10;
        }


        private void ButtonParams_Click(object sender, EventArgs e)
        {
            //https://docs.microsoft.com/zh-tw/dotnet/csharp/language-reference/keywords/params
            //六。避免使用 params 的寫法 -> 
            //    因為....... 
            //         1. hard code for set 很少見 
            //               Fun("1 2 3 4 5");
            //               Fun(new int[] { 1, 2, 3, 4, 5 });
            //         2. 無法雙 set
            //         3. 無法 out 
            //         4. 無法 overloading....
        }

        private void ButtonCheckParameter_Click(object sender, EventArgs e)
        {
            ParameterCheckDemo.SetValue(10);
        }

        private void ButtonRepeatCall_Click(object sender, EventArgs e)
        {
            //你覺得這段方法有什麼問題?? 
            //if(GetPK() == myPK)
            //{
            //    string name = GetName(GetPK());
            //    string id = GetId(GetPK());
            //}
        }

        private void ButtonLocalFunctions_Click(object sender, EventArgs e)
        {
            ////使用區域函式取代lambda以免顯得壅擠

            //List<int> numbers = new List<int>();

            //numbers.Where(number => number > 0).Select(number => number.ToString()).ToString();

            //numbers.ForEach(number => number = number + 10);

            ////    lambda
            ////    ConcurrentDictionary.AddOrUpdate(id,
            ////        key => factory.Create( key ),
            ////		( key, previous ) => 
            ////		{
            ////			if( previous is IDisposable disposable )
            ////			{
            ////				disposable.Dispose();
            ////			}
            ////			return factory.Create( key );
            ////		}
            ////	);

            ////    // Good  
          ////	object DisposeAndUpdate(object key, object previous)
            ////{
            ////    if (previous is IDisposable disposable )
            ////		{
            ////        disposable.Dispose();
            ////    }
            ////    return factory.Create(key);       ////	ConcurrentDictionary.AddOrUpdate( id, factory.Create, DisposeAndUpdate );
       
        }

        private void ButtonMethodLength_Click(object sender, EventArgs e)
        {
            //方法長度不要太長，至少不要超過你的螢幕顯示範圍
        }

        private void ButtonAt_Click(object sender, EventArgs e)
        {
            //跳脫字元，還是要用的話要""
            string s1 = "He said, \"This is the last \u0063hance\x0021\"";
            string s2 = @"He said, ""This is the last \u0063hance\x0021""";









            string goodWay = @"""我""
                                    要換行";
            string badWay = "\"我\""
                                  + "要換行";
        }

        private void ButtonStringConcat_Click(object sender, EventArgs e)
        {
            string head = "頭";
            string tail = "尾";

            string wholeContent = string.Empty;
            wholeContent = head + "中間" + tail;

            wholeContent = string.Format("{0}中間{1}", head, tail);

            wholeContent = $"{head}中間{tail}";





            int loop = 100000;
            string result = string.Empty;
            string word = "abc";
            int st = Environment.TickCount;

            //可讀性較高
            result = string.Empty;
            st = Environment.TickCount;
            for (int i = 0; i < loop; i++)
            {
                result = $"{result}{word}";
            }
            st = Environment.TickCount - st;
            MessageBox.Show(string.Format("使用 $字號 {0} ms", st));

            result = string.Empty;
            st = Environment.TickCount;
            for (int i = 0; i < loop; i++)
            {
                result = result+ word;
            }
            st = Environment.TickCount - st;
            MessageBox.Show(string.Format("使用 +字號 {0} ms", st));


            //for (int i = 0; i < loop; i++)
            //{
            //    result = result + word;
            //}
            //st = Environment.TickCount - st;
            //MessageBox.Show(string.Format("直接加 string {0} ms", st));


            //大量串接字串
            result = string.Empty;
            StringBuilder sb = new StringBuilder();
            st = Environment.TickCount;
            for (int i = 0; i < loop; i++)
            {
                sb.Append(word);
            }
            result = sb.ToString();
            st = Environment.TickCount - st;
            MessageBox.Show(string.Format("使用 string builder {0} ms", st));



        }

        private void ButtonStringEmpty_Click(object sender, EventArgs e)
        {
            string result = "" + "" + "" + "" + " " + "" + "" + "" + " " + "" + "" + " " + "" + "";

            string stringEmpty = string.Empty;

            if (stringEmpty.Equals(""))
            {

            }

            if (stringEmpty.Equals(" "))
            {

            }

            if (stringEmpty.Equals(string.Empty))
            {

            }
        }

        private void ButtonStringCompare_Click(object sender, EventArgs e)
        {
            //String.Compare()  
        }

        private void ButtonStringSearch_Click(object sender, EventArgs e)
        {
            //字串比對是有高階演算法可以作到，所以你應該相信.Net 是有幫你實作的
            //https://zh.wikipedia.org/wiki/%E5%85%8B%E5%8A%AA%E6%96%AF-%E8%8E%AB%E9%87%8C%E6%96%AF-%E6%99%AE%E6%8B%89%E7%89%B9%E7%AE%97%E6%B3%95
        }

        private void ButtonMemoryAllocate_Click(object sender, EventArgs e)
        {
            //Segment
            //Data：存常數、靜態
            //優：一開始就配好記憶體，效能佳
            //缺：無生命週期，就算沒有使用到也會佔用記憶體直到程式結束

            //Code

            //Stack：ValueType的值、ReferenceType在Heap的位置
            //有明確生命週期，記憶體自行釋放

            //Heap：ReferenceType的值
            //等待GC

            //32 bit(2G) 
            //64 bit(4G)
        }

        private void ButtonException_Click(object sender, EventArgs e)
        {
            //CPU cache memory
            //Ram catch code

            //攔截例外的代價，遠超過你的想像~~!!

            //一。警告!! 禁止使用 TryCatch 來控制流程 (Exception Driven)
            //    也就是說，程式流程裡，不可以有 「試著跑跑看，如果發生例外則執行流程A，否則就執行流程B」

            //二。只攔下你可以處理的例外，不要什麼都攔，一但攔下來你就應該作出相對應的處理
            //    不應該有空的例外攔截而無任何處理碼，這樣只是遮掩錯誤，埋下不動作的種子        

            //三。可以自訂自已的專屬例外，但須要注意是否預設的例外就有你需要的功能，不要重複定義
            //    context information包含例外發生時物件內各種關鍵的變數值，傳入的參數等能夠幫助你除錯的內容，
            //    此外切記要將真正的Exception放入自訂Exception的innerException，避免重要除錯資料流失

            //四。不要作巢狀的例外攔截

            //五。不要攔下來都不作什麼又把它往外丟，這樣不如不要欄

            //六。finally 區塊只是用清空資源，不該把其他不相關的流程行為寫在這裡

            //七。Time sharing + Process control block 說明例外的成本

            //https://msdn.microsoft.com/zh-tw/library/system.exception(v=vs.110).aspx

            //http://www.monitis.com/blog/improving-net-application-performance-part-10-exception-management/
        }

        private void ButtonGC_Click(object sender, EventArgs e)
        {
            //你應該靠 GC ，但不該無視 GC，他的成本會隨著規模放大(定時、定量)
            //https://msdn.microsoft.com/en-us/library/ee787088(v=vs.110).aspx
        }

        private void ButtonThreadSafe_Click(object sender, EventArgs e)
        {
            //沒事不要 new thread (Race condition)
            //多執行緒的世界是需要 Thread-Safe ( Dead Lock 、 Static ? 、 ConcurrentDictionary ? )
        }

        private void ButtonRaceCondition_Click(object sender, EventArgs e)
        {

        }

        private void ButtonExtendMethod_Click(object sender, EventArgs e)
        {
            //透過一個靜態類別裡的靜態方法去實作想要的功能
        }

        private void ButtonGoto_Click(object sender, EventArgs e)
        {
            //禁用 goto 語法
            //不好追蹤，去了就回不來，或造成意想不到的結果
        }

        private void ButtonDispose_Click(object sender, EventArgs e)
        {
            //若你使用實作IDisposable的物件, 請總是使用using或調用Dispose釋放資源
            //請總是確認你使用的物件是否實作IDisposable, 並在使用完畢後呼叫dispose或使用using關鍵字, 
            //未正確釋放資源可能造成記憶體洩漏等問題



            using (Stream stream = File.Open("path", FileMode.Open))
            {
                using (StreamReader reader = new StreamReader(stream))
                {
                }
            }
        }

        private void ButtonDoEvents_Click(object sender, EventArgs e)
        {
            //禁用 Application.DoEvents 方法
            // 首先，你需要知道 main thread https://users.physics.ox.ac.uk/~Steane/cpp_help/winapi_intro.htm
            // 然後，看看 MSDN https://msdn.microsoft.com/en-us/library/system.windows.forms.application.doevents.aspx
            // 最後，看看別人怎麼說它的 http://blog.codinghorror.com/is-doevents-evil/
        }

        private void ButtonCSharp_Click(object sender, EventArgs e)
        {
            //C#三大特性
            //1.跨平台：.NET Framework、.NET Core、XAMARIN，後兩者才能真的跨平台
            //2.強型別
            //3.物件導向的程式語言

            //有著承先啟後的願景
        }

        private void ButtonDelegateAndEvent_Click(object sender, EventArgs e)
        {
            //http://192.168.99.115/books/coding-style/page/%E5%A7%94%E6%B4%BE
        }

        private void buttonObjectOriented_Click(object sender, EventArgs e)
        {
            //物件導向 VS. 非物件導向
            //非物件導向：「中央集權」，利用條件判斷function call function，牽一髮而動全身
            //物件導向：「地方自治」，透過把包裝的概念降低資訊的能見度，進而降低彼此依賴的程度

            ProcessOriented();
            ObejctOriented();

            //物件導向三大特性
        }

        private void ProcessOriented()
        {
            Tuple<string, string> jason = new Tuple<string, string>("Jason", "Student");
            Tuple<string, string> kevin = new Tuple<string, string>("Kevin", "Engineer");

            List<Tuple<string, string>> people = new List<Tuple<string, string>>();
            people.Add(jason);
            people.Add(kevin);


            foreach (var person in people)
            {
                if (person.Item2 == "Student")
                {
                    string 職業 = $"{person.Item1} 是個學生";
                    string 起床 = $"{person.Item1}06:00起床";

                }
                else if (person.Item2 == "Engineer")
                {
                    string 職業 = $"{person.Item1} 是個工程師";
                    string 起床 = $"{person.Item1}08:00起床";
                }

                string 喝水 = $"{person.Item1}喝水";

            }

        }

        private void ObejctOriented()
        {
            Student jason = new Student("Jason");

            Engineer kevin = new Engineer("Kevin");

            List<Person> people = new List<Person>() { jason,kevin};


            foreach (Person person in people)
            {
                string 職業 = person.GetCareer();
                string 起床 = person.WakeUp();
                string 喝水 = person.DrinkWater();
            }

            string 學生上課 = jason.Learn();
            string 工程師上班 = kevin.Work();

            bool 減肥 = true;
            string 一般時期午餐 = kevin.EatLunch();
            string 減肥時期午餐 = kevin.EatLunch(減肥);
        }

        private void buttonSolution_Click(object sender, EventArgs e)
        {
            //方案->專案->組件->命名空間->CS檔->類別->類別成員
            
        }
    }
}

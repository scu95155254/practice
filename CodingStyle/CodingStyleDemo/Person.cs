﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    public abstract class Person
    {
        protected string Name;

        protected string Career;

        protected Person(string value)
        {
            Name = value;
        }

        internal string GetCareer()
        {
            return $"{Name}是個{Career}";
        }
        internal string DrinkWater()
        {
            return $"{Name}在喝水";
        }

        internal abstract string WakeUp();

        internal string EatLunch()
        {
            return $"{Name}吃午餐";
        }


    }
}

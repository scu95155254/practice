﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class MagicNumber
    {
        private void SetDayOffMagicNumber(int day)
        {
            //在沒有特別註明的情況下，直接在程式碼內寫具體數值
            if (day == 6)
            {
                //DayOff
            }
        }

        private void SetDayOffEnum(int day)
        {
            if (day == (int)DayOfWeek.Saturday)
            {
                //DayOff
            }
        }

        #region Const
        private const int Saturday = 6;
        private void SetDayOffConst(int day)
        {
            if (day == Saturday)
            {
                //DayOff
            }
        }
        #endregion

        private void SetDayOffVariable(int day)
        {
            int saturday = 6;
            if (day == saturday)
            {
                //DayOff
            }
        }
    }
}

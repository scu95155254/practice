﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class CurlyBrackets
    {
        /// <summary>
        /// 任何大括號"獨佔一行"!
        /// </summary>
        internal static void SingleLine()
        {
            DayOfWeek dayOfWeek = DayOfWeek.Monday;

            //Use it
            if (dayOfWeek == DayOfWeek.Monday)
            {

            }

            //Don't use it
            //if (dayOfWeek == DayOfWeek.Monday){

            //}
        }

        /// <summary>
        /// 判斷後一定要把執行範圍用大括號括起來
        /// </summary>
        internal static void IfMustAddCurlyBrackets()
        {
            DayOfWeek dayOfWeek = DayOfWeek.Saturday;

            //正確
            if (dayOfWeek == DayOfWeek.Monday)
            {
                BuyBreakfast();
                GoToWork();
            }



            //錯誤
            if (dayOfWeek == DayOfWeek.Monday)
                BuyBreakfast();
                GoToWork();

            for (int i = 1; i < 10; i++)
                GoToWork();
                BuyBreakfast();

            //https://www.inside.com.tw/article/3541-behind-apple-ios-7-0-6-update
        }

        /// <summary>
        /// 買早餐
        /// </summary>
        private static void BuyBreakfast()
        {
            //TODO：用RichTextBox Show 不要用偵錯看
        }

        /// <summary>
        /// 上班
        /// </summary>
        private static void GoToWork()
        {
            //TODO：用RichTextBox Show 不要用偵錯看
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class Engineer : Person
    {
        public Engineer(string value) : base(value)
        {
            Career = "工程師";
            //Career ="工程師AAA";
        }

        internal override string WakeUp()
        {
            return $"{Name}08:00起床";
        }

        internal string Work()
        {
            return $"{Name} 在辦公室Coding";
        }

        internal string EatLunch(bool isOnDiet)
        {
            string result = string.Empty;
            if(isOnDiet)
            {
                result = $"{Name}減肥不吃午餐";
            }
            else
            {
                result = $"{Name}吃午餐";
            }
            return result;
        }
    }
}

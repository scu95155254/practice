﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    /// <summary>
    /// 共用項目(方便大家使用，降低重複性)
    /// </summary>
    internal static class Global
    {
        /// <summary>
        /// 公司信箱
        /// </summary>
        internal const string ServerEmail = "cm@cmoney.net.tw";

        /// <summary>
        /// 寄信
        /// </summary>
        /// <param name="email">信箱地址</param>
        /// <param name="content">信件內容</param>
        internal static void SendMail(string email,string content)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class Student : Person
    {
        internal Student(string value) : base(value)
        {
            Career = "學生";

        }
        internal override string WakeUp()
        {
            return $"{Name}06:00起床";
        }

        internal string Learn()
        {
            return $"{Name} 在學校上課";
        }
    }
}

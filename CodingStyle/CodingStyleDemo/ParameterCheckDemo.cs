﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class ParameterCheckDemo
    {
        private const int MinValue = 0;
        internal static void SetValue(int value)
        {
            //內部可控的情況下不要重複檢查
            //小於0
            if (value < MinValue)
            {
                return;
            }
            SetValueInner(value);
        }

        private static void SetValueInner(int value)
        {
            if (value < MinValue)
            {
                return;
            }
        }
    }
}

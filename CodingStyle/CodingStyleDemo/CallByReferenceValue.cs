﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class CallByReferenceValue
    {
        internal static void SetValue(int value)
        {
            value = 999;
        }

        internal static void SetValue(ref int value)
        {
            value = 888;
        }

        internal static void SetValue(Food food)
        {
            food.Name = "漢堡";
        }

        internal static void SetValueNew(Food food)
        {
            food = new Food();
        }

        internal static void SetValue(ref Food food)
        {
            food.Name = "被改值囉";
        }

        internal static void SetValueNew(ref Food food)
        {
            food = new Food();
        }

        internal static void SetValue(string name)
        {
            name = "Tom";
        }

        internal static void SetValue(ref string name)
        {
            name = "Tom";
        }
    }
}

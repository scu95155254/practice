﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodingStyleDemo
{
    internal class Restaurant
    {
        internal Restaurant()
        {
            List<Food> foods = new List<Food>();
        }
    }

    /// <summary>
    /// Food POCO
    /// </summary>
    internal class Food
    {
        internal string Name { get; set; }
        internal int Price { get; set; }
    }

    /// <summary>
    /// Drink POCO
    /// </summary>
    class Drink
    {
        string Name { get; set; }
        int Price { get; set; }
    }
}

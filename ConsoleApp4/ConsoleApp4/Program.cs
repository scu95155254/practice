﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace ConsoleApp4
{
    class Program
    {
        public static object PhantomJSDriver { get; private set; }

        static void Main(string[] args)
        {

            DataTable dataTable = new DataTable();
            dataTable.Columns.Add("ID", typeof(string));
            dataTable.Columns.Add("Name", typeof(string));
            dataTable.Columns.Add("Math");
            dataTable.Columns.Add("Eng");
            dataTable.Columns.Add("Sci");

            DataRow dataRow = dataTable.NewRow();
            dataRow.ItemArray = new string[] { "1", "apple", "90", "-", "-" };
            dataTable.Rows.Add(dataRow);
            DataRow dataRow2 = dataTable.NewRow();
            dataRow2.ItemArray = new string[] { "1", "apple", "-", "80", "70" };
            dataTable.Rows.Add(dataRow2);

            DataRow dataRow3 = dataTable.NewRow();
            dataRow3.ItemArray = new string[] { "2", "boy", "-", "50", "60" };
            dataTable.Rows.Add(dataRow3);

            DataRow dataRow4 = dataTable.NewRow();
            dataRow4.ItemArray = new string[] { "2", "boy", "40", "-", "-" };
            dataTable.Rows.Add(dataRow4);
            //-------------------------------------------------------------------

            var a = dataTable.AsEnumerable().Where(row => row["Math"].Equals("-")).CopyToDataTable();
            var b = dataTable.AsEnumerable().Where(row => row["Eng"].Equals("-")).CopyToDataTable();

            //var cc = a.Join(b, c => c["ID"], d => d["ID"], (c, d) => new { id = c["ID"], name = c["Name"], math = d["Math"], eng = c["Eng"], sci = c["Sci"] }).ToArray();
            //DataRow newRow = dataTable.NewRow();
            //newRow.ItemArray = ;
            //dataTable.Rows.Add(newRow);










            Console.Read();

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace practice1108
{
    class Program
    {
        /// <summary>
        /// 連線登入
        /// </summary>
        private const string CONNECTION_INFO = "Data Source=192.168.10.180; Database=StockDB; User ID=test; Password=test";

        static void Main(string[] args)
        {
            using (SqlConnection connection=new SqlConnection(CONNECTION_INFO))
            {
                connection.Open();
                SqlCommand sqlComn = new SqlCommand
                {
                    Connection = connection
                };

                string createTempTable = "DECLARE @Table_Stock AS TABLE(投票日期 char(8), 證券代號 varchar(10), 證券名稱 nvarchar(20), 召集人 nvarchar(50), 股東會日期 char(8), 發行代理機構 nvarchar(20), 聯絡電話 varchar(20), MTIME int)";
                sqlComn.CommandText = createTempTable;
                sqlComn.ExecuteNonQuery();

                using (SqlBulkCopy sqlBulkCopy=new SqlBulkCopy(connection))
                {
                    sqlBulkCopy.DestinationTableName = "@Table_Stock";
                    sqlBulkCopy.WriteToServer()
                }

            }


                Console.ReadKey();
        }
    }
}
